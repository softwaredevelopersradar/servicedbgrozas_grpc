﻿using System;
using System.Linq;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationOwnUAVFreqSignal : OperationTableDb<TableOwnUavFreqSignal>, IDependentOwnUavFreq
    {
        public TableOwnUavFreqSignal LoadByFilter( int ownUavFreqId, int idClient)
        {
            TableOwnUavFreqSignal data = new TableOwnUavFreqSignal();
            try
            {
                lock (DataBase)
                {
                    DbSet<TableOwnUavFreqSignal> Table = DataBase.GetTable<TableOwnUavFreqSignal>(Name);
                    if (DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq).Find(ownUavFreqId) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchOwnFreqIsAbsent);

                    //var properties = typeof(TableOwnUAVFreq).GetProperties();
                    //foreach (var property in properties)
                    //{
                    //    if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                    //    {
                    //        // если найдена, то испльзуем жадную загрузку связных таблиц
                    //        return ClassDataDependASP.ConvertToDataDependASP(Table.Where(t => (t as T).NumberASP == NumberASP).Include(property.Name).ToList());
                    //    }
                    //}
                    return Table.Where(t => (t as TableOwnUavFreqSignal).TableOwnUAVFreqId == ownUavFreqId).ToList().Last();
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var recordOwnUAVFreq = DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq).Find((record as TableOwnUavFreqSignal).TableOwnUAVFreqId);

                    if (recordOwnUAVFreq == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchOwnFreqIsAbsent);
                    }
                    
                   
                }

                DbSet<TableOwnUavFreqSignal> Table = DataBase.GetTable<TableOwnUavFreqSignal>(Name);

                //var key = Table.Find(record.GetKey());
                var anotherRecForFreq = Table.Where(t=>t.TableOwnUAVFreqId == (record as TableOwnUavFreqSignal).TableOwnUAVFreqId).FirstOrDefault();
                
                if (anotherRecForFreq == null)
                {
                    base.Add(record, idClient);
                }
                else 
                {
                    (record as TableOwnUavFreqSignal).Id = anotherRecForFreq.Id;
                    base.Change(record, idClient); 
                }

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var recordOwnUAVFreq = DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq).Find((record as TableOwnUavFreqSignal).TableOwnUAVFreqId);

                    if (recordOwnUAVFreq == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchOwnFreqIsAbsent);
                    }
                }
                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void ClearByFilter(int ownUavFreqId, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableOwnUavFreqSignal>(Name);

                    if (DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq).Find(ownUavFreqId) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchOwnFreqIsAbsent);


                    Table.RemoveRange(Table.Where(t => t.TableOwnUAVFreqId == ownUavFreqId).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}

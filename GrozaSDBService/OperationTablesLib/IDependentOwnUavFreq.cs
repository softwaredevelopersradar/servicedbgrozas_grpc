﻿using GrozaSModelsDBLib;

namespace OperationTablesLib
{
    public interface IDependentOwnUavFreq
    {
        TableOwnUavFreqSignal LoadByFilter(int ownUavFreqId, int idClient);
        void ClearByFilter(int ownUavFreqId, int idClient);
    }
}

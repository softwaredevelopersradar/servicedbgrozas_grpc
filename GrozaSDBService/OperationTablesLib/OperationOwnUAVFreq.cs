﻿using System;
using System.Linq;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationOwnUAVFreq : OperationTableDb<TableOwnUAVFreq>
    {
        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                using (TablesContext db = new TablesContext())
                {
                    var Table = db.GetTable<TableOwnUAVFreq>(Name);
                    data = ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());
                }

                return data;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}

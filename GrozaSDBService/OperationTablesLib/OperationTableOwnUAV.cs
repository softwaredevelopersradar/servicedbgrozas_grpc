﻿using System;
using System.Linq;
using Errors;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationTableOwnUAV : OperationTableDb<TableOwnUAV>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var record in data.ListRecords)
                {
                    (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                }
                base.AddRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {

                    var Table = DataBase.GetTable<TableOwnUAV>(Name);

                    TableOwnUAV rec = Table.Include(t => t.Frequencies).AsNoTracking().Where(r => r.SerialNumber == (record as TableOwnUAV).SerialNumber).FirstOrDefault();
                    record.Id = rec != null ? rec.Id : record.Id;
                    //(record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                }
                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableOwnUAV>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        TableOwnUAV rec = Table.Include(t => t.Frequencies).AsNoTracking().Where(r => r.SerialNumber == (record as TableOwnUAV).SerialNumber).FirstOrDefault();
                        record.Id = rec != null ? rec.Id : record.Id;
                        //(record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                    }
                }
                //foreach (var record in data.ListRecords)
                //{
                //    (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                //}
                base.RemoveRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {
        //        TableOwnUAV rec;
        //        lock (DataBase)
        //        {
        //            using (TablesContext db = new TablesContext())
        //            {
        //                DbSet<TableOwnUAV> tableSource = DataBase.GetTable<TableOwnUAV>(Name);
        //                var k = (record as TableOwnUAV).GetKey();
        //                rec = tableSource.AsNoTracking().Where(r => r.Id == (record as TableOwnUAV).Id).FirstOrDefault();
        //                //TableOwnUAV rec = tableSource.Find(record.GetKey());
        //                if (rec == null)
        //                    throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

        //                //DbSet<TableOwnUAVFreq> tableFreqs = DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq);
        //                //var forDeleteId = rec.Frequencies.Select(u => u.Id).ToList().Except((record as TableOwnUAV).Frequencies.Select(u => u.Id).ToList());/*.Select(t=> new TableOwnUAVFreq() { Id = t })*/
        //                ////tableFreqs.RemoveRange(forDelete);
        //                ////DataBase.SaveChanges();
        //                //var forDelete = rec.Frequencies.Where(t => forDeleteId.Contains(t.Id)).ToList();

        //                (rec as AbstractCommonTable).Update(record);

        //                db.Entry(rec).State = EntityState.Modified;
        //                //db.TOwnUAVFreq.RemoveRange(forDelete);
        //                //db.SaveChanges();

        //                if (rec.Frequencies != null)
        //                {
        //                    foreach (var freq in rec.Frequencies)
        //                    {
        //                        db.Entry(freq).State = freq.Id == 0 ? EntityState.Added : EntityState.Modified;
        //                    }
        //                }

        //                db.SaveChanges();
        //                db.Entry(rec).State = EntityState.Detached;
        //            }
        //            //tableSource.Remove(rec);
        //            //DataBase.SaveChanges();
        //            //(rec as AbstractCommonTable).Update(record);
        //            //tableSource.Add(rec);
        //            //DataBase.SaveChanges();
        //        }

        //        UpDate(idClient);

        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                TableOwnUAV rec;
                lock (DataBase)
                {
                    using (TablesContext db = new TablesContext())
                    {
                        DbSet<TableOwnUAV> tableSource = DataBase.GetTable<TableOwnUAV>(Name);
                        var k = (record as TableOwnUAV).GetKey();
                        rec = tableSource.Include(t => t.Frequencies).AsNoTracking().Where(r => r.Id == (record as TableOwnUAV).Id).FirstOrDefault();
                        //TableOwnUAV rec = tableSource.Find(record.GetKey());
                        if (rec == null)
                            throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                        DbSet<TableOwnUAVFreq> tableFreqs = DataBase.GetTable<TableOwnUAVFreq>(NameTable.TableOwnUAVFreq);
                        var forDeleteId = rec.Frequencies.Select(u => u.Id).ToList().Except((record as TableOwnUAV).Frequencies.Select(u => u.Id).ToList());
                        var forDelete = rec.Frequencies.Where(t => forDeleteId.Contains(t.Id)).ToList();

                        (rec as AbstractCommonTable).Update(record);
                    
                        db.Entry(rec).State = EntityState.Modified;
                        db.TOwnUAVFreq.RemoveRange(forDelete);
                        db.SaveChanges();

                        if (rec.Frequencies != null)
                        {
                            foreach (var freq in rec.Frequencies)
                            {
                                db.Entry(freq).State = freq.Id == 0 ? EntityState.Added : EntityState.Modified;
                            }
                        }

                        db.SaveChanges();
                        db.Entry(rec).State = EntityState.Detached;
                    }
                }

                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                using (TablesContext db = new TablesContext())
                {
                    var Table = db.GetTable<TableOwnUAV>(Name);
                    data = ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Frequencies).ToList());
                }


                return data;
                //lock (DataBase)
                //{
                //    var Table = DataBase.GetTable<TableOwnUAV>(Name);

                //    var temp = ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Frequencies).ToList());
                //    return temp;
                //}
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}

﻿using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationGlobalProperties : OperationTableDb<GlobalProperties>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                GlobalProperties recFind;
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);
                    recFind = Table.Find(record.GetKey());
                }

                (record as GlobalProperties).RadioIntelegence.ZoneAlarm = CheckMaxValue((record as GlobalProperties).RadioIntelegence.ZoneAlarm);
                (record as GlobalProperties).RadioIntelegence.ZoneAttention = CheckMaxValue((record as GlobalProperties).RadioIntelegence.ZoneAttention);
                (record as GlobalProperties).RadioIntelegence.ZoneReadiness = CheckMaxValue((record as GlobalProperties).RadioIntelegence.ZoneReadiness);
                (record as GlobalProperties).RadioIntelegence.DistanceIntelegence = CheckDistanceMaxValue((record as GlobalProperties).RadioIntelegence.DistanceIntelegence);
                (record as GlobalProperties).Jamming.DistanceJamming = CheckMaxValue((record as GlobalProperties).Jamming.DistanceJamming);

                if (recFind != null)
                    base.Change(record, idClient);
                else
                    base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }

        private int CheckMaxValue(int value)
        {
            if (value > 100_000)
                return 100_000;
            else
                return value;
        }

        private int CheckDistanceMaxValue(int value)
        {
            if (value > 400_000)
                return 400_000;
            else
                return value;
        }

    }
}

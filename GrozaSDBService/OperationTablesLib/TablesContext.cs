﻿using System;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableJammerStation> TJammerStation { get; set; }
        public DbSet<TableFreqKnown> TFreqKnown { get; set; }
        public DbSet<TableFreqForbidden> TFreqForbidden { get; set; }
        public DbSet<TableFreqRangesRecon> TFreqRangesRecon { get; set; }
        public DbSet<TableSource> TSource { get; set; }
        public DbSet<TableTrack> TTrack { get; set; }
        public DbSet<TableJamBearing> TJamBearing { get; set; }
        public DbSet<GlobalProperties> TGlobalProperties { get; set; }
        public DbSet<TableSectorsRecon> TSectorsRecon { get; set; }
        public DbSet<TableSuppressSource> TSuppressSource { get; set; }
        public DbSet<TableSuppressGnss> TSuppressGnss { get; set; }
        public DbSet<TablePattern> TPattern { get; set; }
        public DbSet<TableOwnUAV> TOwnUAV { get; set; }
        public DbSet<TableOwnUAVFreq> TOwnUAVFreq { get; set; }
        public DbSet<TableOwnUavFreqSignal> TOwnUAVFreqSignal { get; set; }
        public DbSet<TableCuirasseMPoints> TCuirasseMPoints { get; set; }

        public DbSet<TableOperatorGun> TOperatorGun { get; set; }
        

        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableJammerStation:
                    return TJammerStation as DbSet<T>;
                case NameTable.TableFreqRangesRecon:
                    return TFreqRangesRecon as DbSet<T>;
                case NameTable.TableFreqKnown:
                    return TFreqKnown as DbSet<T>;
                case NameTable.TableFreqForbidden:
                    return TFreqForbidden as DbSet<T>;
                case NameTable.TableSource:
                    return TSource as DbSet<T>;
                case NameTable.TableTrack:
                    return TTrack as DbSet<T>;
                case NameTable.TableJamBearing:
                    return TJamBearing as DbSet<T>;
                case NameTable.GlobalProperties:
                    return TGlobalProperties as DbSet<T>;
                case NameTable.TableSectorsRecon:
                    return TSectorsRecon as DbSet<T>;
                case NameTable.TableSuppressSource:
                    return TSuppressSource as DbSet<T>;
                case NameTable.TableSuppressGnss:
                    return TSuppressGnss as DbSet<T>;
                case NameTable.TablePattern:
                    return TPattern as DbSet<T>;
                case NameTable.TableOwnUAV:
                    return TOwnUAV as DbSet<T>;
                case NameTable.TableOwnUAVFreq:
                    return TOwnUAVFreq as DbSet<T>;
                case NameTable.TableOwnUAVFreqSignal:
                    return TOwnUAVFreqSignal as DbSet<T>;
                case NameTable.TableCuirasseMPoints:
                    return TCuirasseMPoints as DbSet<T>;
                case NameTable.TableOperatorGun:
                    return TOperatorGun as DbSet<T>;
                default:
                    return null;
            }
        }

        public TablesContext()
        {
            try
            {
               SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                var a = ex;
                Console.Write($"Error: {ex.Message}");
            }

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            string path = "Filename=" + System.IO.Directory.GetCurrentDirectory() + "\\GrozaSDB.db";
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Coord>().Property(rec => rec.Altitude).HasDefaultValue(-1);
            //modelBuilder.Entity<Coord>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            //modelBuilder.Entity<Coord>().Property(rec => rec.Longitude).HasDefaultValue(-1);

            modelBuilder.Entity<TableJammerStation>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableJammerStation>().OwnsOne(t => t.Coordinates);


            modelBuilder.Entity<TableFreqRangesRecon>().HasOne<TableJammerStation>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqForbidden>().HasOne<TableJammerStation>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableFreqKnown>().HasOne<TableJammerStation>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableSectorsRecon>().HasOne<TableJammerStation>().WithMany().HasForeignKey(rec => rec.NumberASP).OnDelete(DeleteBehavior.Cascade);

            //modelBuilder.Entity<TableOwnUAV>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableOwnUAVFreq>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableOwnUAV>().HasMany(rec => rec.Frequencies).WithOne().HasForeignKey(rec => rec.TableOwnUAVId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableOwnUavFreqSignal>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableOwnUavFreqSignal>().HasOne<TableOwnUAVFreq>().WithOne().OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableSource>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableSuppressSource>().Property(rec => rec.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<TableJamBearing>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrack>().HasMany(rec => rec.Bearing).WithOne().HasForeignKey(rec => new { rec.TableTrackId, rec.TableSourceId }).HasPrincipalKey(rec => new { rec.Id, rec.TableSourceId })
                .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<TableSource>().HasMany(rec => rec.Track).WithOne().HasForeignKey(rec => rec.TableSourceId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableTrack>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableTrack>().Property(rec => rec.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<OperatingFrequency>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TablePattern>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TablePattern>().HasMany(rec => rec.FrequencyList).WithOne().OnDelete(DeleteBehavior.Cascade);

            //modelBuilder.Entity<ParamCmp>().Property(rec => rec.Angle).HasDefaultValue(0);

            //modelBuilder.Entity<ParamOEM>().Property(rec => rec.Distance).HasDefaultValue(7000);

            modelBuilder.Entity<TableCuirasseMPoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableCuirasseMPoints>().OwnsOne(t => t.Coordinates);

            

           

            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Gnss);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.CmpRX);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.CmpTX);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Oem);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.RadioIntelegence);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Jamming);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Spoofing);

            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.ZoneAttention).HasDefaultValue(50000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.ZoneReadiness).HasDefaultValue(20000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.ZoneAlarm).HasDefaultValue(10000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.DistanceIntelegence).HasDefaultValue(400_000);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.BearingAccuracy).HasDefaultValue(22.5);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.FrequencyAccuracy).HasDefaultValue(5);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.StaticAngle).HasDefaultValue(-1);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.BandAccuracy).HasDefaultValue(1);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.BandLargerError).HasDefaultValue(10);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.BandLessError).HasDefaultValue(10);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.SSB).HasDefaultValue(TSSB.Band160);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.NumOfScans).HasDefaultValue(6);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.ScanChannel).HasDefaultValue(ScanChannels.Ch_1);
            //modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.DetectionChannel).HasDefaultValue(ScanChannels.Ch_1);

            //modelBuilder.Entity<Jamming>().Property(rec => rec.TimeRadiat).HasDefaultValue(3);
            //modelBuilder.Entity<Jamming>().Property(rec => rec.Power100_500).HasDefaultValue(125);
            //modelBuilder.Entity<Jamming>().Property(rec => rec.Power500_2500).HasDefaultValue(50);
            //modelBuilder.Entity<Jamming>().Property(rec => rec.Power2500_6000).HasDefaultValue(10);
            //modelBuilder.Entity<Jamming>().Property(rec => rec.DistanceJamming).HasDefaultValue(50000);
            //modelBuilder.Entity<Jamming>().Property(rec => rec.Sector).HasDefaultValue(60);

            modelBuilder.Entity<GlobalProperties>(b =>
            {
                b.HasData(new
                {
                    Id = 1
                });

                b.OwnsOne(e => e.Gnss).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Altitude = (float)-1,
                    Latitude = (double)-1,
                    Longitude = (double)-1
                });

                b.OwnsOne(e => e.Spoofing).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Altitude = (float)-1,
                    Latitude = (double)-1,
                    Longitude = (double)-1
                });

                b.OwnsOne(e => e.CmpRX).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Angle = (float)0
                });

                b.OwnsOne(e => e.CmpTX).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Angle = (float)0
                });

                b.OwnsOne(e => e.Oem).HasData(new
                {
                    GlobalPropertiesId = 1,
                    Distance = 7000
                });

                b.OwnsOne(e => e.RadioIntelegence).HasData(new
                {
                    GlobalPropertiesId = 1,
                    ZoneAttention = 50000,
                    ZoneReadiness = 20000,
                    ZoneAlarm = 10000,
                    DistanceIntelegence = 400_000,
                    BearingAccuracy = (float)22.5,
                    FrequencyAccuracy = (float)5,
                    StaticAngle = (float)-1,
                    BandAccuracy = (float)2,
                    BandLargerError = (float)10,
                    BandLessError = (float)10,
                    SSB = (TSSB)1,
                    NumOfScans = (byte)6,
                    ScanChannel = (ScanChannels)1,
                    DetectionChannel = (ScanChannels)1
                });

                b.OwnsOne(e => e.Jamming).HasData(new
                {
                    GlobalPropertiesId = 1,
                    TimeRadiat = (short)3,
                    Power100_500 = (short)125,
                    Power500_2500 = (short)50,
                    Power2500_6000 = (short)10,
                    DistanceJamming = 50000,
                    Sector = (float)60
                });
            });


            modelBuilder.Entity<TableSuppressSource>().HasData(
                new TableSuppressSource(){ Id = 11 }, new TableSuppressSource() { Id = 12 }, new TableSuppressSource() { Id = 13 }, 
                new TableSuppressSource() { Id = 14 }, new TableSuppressSource() { Id = 21 }, new TableSuppressSource() { Id = 22 }, 
                new TableSuppressSource() { Id = 23 }, new TableSuppressSource() { Id = 24 });


            modelBuilder.Entity<TableSuppressGnss>().HasData(
                new TableSuppressGnss() { Id = 11, Type = TypeGNSS.Gps }, new TableSuppressGnss() { Id = 12, Type = TypeGNSS.Glonass }, 
                new TableSuppressGnss() { Id = 13, Type = TypeGNSS.Beidou }, new TableSuppressGnss() { Id = 14, Type = TypeGNSS.Galileo },
                new TableSuppressGnss() { Id = 21, Type = TypeGNSS.Gps }, new TableSuppressGnss() { Id = 22, Type = TypeGNSS.Glonass },
                new TableSuppressGnss() { Id = 23, Type = TypeGNSS.Beidou }, new TableSuppressGnss() { Id = 24, Type = TypeGNSS.Galileo });
        }
    }
}

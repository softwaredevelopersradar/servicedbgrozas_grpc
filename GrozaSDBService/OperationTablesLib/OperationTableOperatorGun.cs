﻿using Errors;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationTablesLib
{
    public class OperationTableOperatorGun : OperationTableDb<TableOperatorGun>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {

                lock (DataBase)
                {
                    DbSet<TableOperatorGun> tableSource = DataBase.GetTable<TableOperatorGun>(Name);

                    TableOperatorGun rec = tableSource.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);


                    tableSource.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    tableSource.Add(rec);
                    DataBase.SaveChanges();
                }

                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}

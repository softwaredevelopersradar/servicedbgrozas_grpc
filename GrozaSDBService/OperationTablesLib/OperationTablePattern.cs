﻿using System;
using System.Linq;
using Errors;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class OperationTablePattern : OperationTableDb<TablePattern>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {

                lock (DataBase)
                {
                    DbSet<TablePattern> tableSource = DataBase.GetTable<TablePattern>(Name);

                    TablePattern rec = tableSource.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);


                    tableSource.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    tableSource.Add(rec);
                    DataBase.SaveChanges();
                }

                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}

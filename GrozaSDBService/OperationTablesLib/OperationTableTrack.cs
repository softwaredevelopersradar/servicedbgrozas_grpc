﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using Microsoft.EntityFrameworkCore;
using Errors;

namespace OperationTablesLib
{
    public class OperationTableTrack : OperationTableDb<TableTrack>
    {
        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                List<TableTrack> AddRange = new List<TableTrack>();
                lock (DataBase)
                {
                    DbSet<TableTrack> Table = DataBase.GetTable<TableTrack>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableTrack).Bearing != null)
                            {
                                DbSet<TableJamBearing> tableJamBearing = DataBase.TJamBearing;
                                tableJamBearing.UpdateRange(rec.Bearing);
                            }
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableTrack);
                            AddRange.Add(record as TableTrack);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                List<TableTrack> ChangeRange = new List<TableTrack>();
                lock (DataBase)
                {
                    DbSet<TableTrack> Table = DataBase.GetTable<TableTrack>(Name);

                    TableTrack rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    (rec as AbstractCommonTable).Update(record);

                    if (rec.Bearing != null)
                    {
                        DbSet<TableJamBearing> tableJamBearing = DataBase.TJamBearing;
                        tableJamBearing.RemoveRange(tableJamBearing.Where(c => c.TableTrackId == rec.Id).Where(c => c.TableSourceId == rec.TableSourceId).ToList());
                        DataBase.SaveChanges();
                    }
                    Table.Update(rec);
                    DataBase.SaveChanges();
                    //ChangeRange.Add(rec);
                    //Table.Update(rec);
                    //if (rec.Bearing != null)
                    //{
                    //    DbSet<TableJamBearing> tableJamBearing = DataBase.TJamBearing;
                    //    tableJamBearing.UpdateRange(rec.Bearing);
                    //}
                    //DataBase.SaveChanges();
                }
                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}

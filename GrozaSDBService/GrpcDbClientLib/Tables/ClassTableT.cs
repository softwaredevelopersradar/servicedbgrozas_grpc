﻿using System;
using System.Collections.Generic;
using InheritorsEventArgs;
using GrozaSModelsDBLib;
using System.Threading.Tasks;
using GrozaSDbPackage;
using ProtoGSLib;
using Grpc.Core;

namespace GrpcDbClientLib
{
    internal class ClassTable<T> : ClassTable, IClassTables, IClickUpData, IClickUpRecord, IClickUpAddRange, ITableUpRecord<T>, ITableAddRange<T>, ITableUpdate<T> where T : AbstractCommonTable
    {
        protected NameTable Name;
        #region ITableAddRange

        public virtual event EventHandler<TableEventArgs<T>> OnAddRange;

        public virtual void ClickUpAddRange(ClassDataCommon dataCommon)
        {
            OnAddRange?.Invoke(this, new TableEventArgs<T>(dataCommon));
        }
        #endregion

        #region UpFullData

        public virtual event EventHandler<TableEventArgs<T>> OnUpTable;

        public virtual void ClickUpTable(ClassDataCommon data)
        {
            OnUpTable?.Invoke(this, new TableEventArgs<T>(data));
        }

        #endregion

        #region UpRecord

        public virtual event EventHandler<T> OnAddRecord;

        public virtual event EventHandler<T> OnDeleteRecord;

        public virtual event EventHandler<T> OnChangeRecord;


        public virtual void ClickUpRecord(InheritorsEventArgs.RecordEventArgs eventArgs)
        {
            switch (eventArgs.NameAction)
            {
                case NameChangeOperation.Add:
                    OnAddRecord?.Invoke(this, eventArgs.AbstractRecord as T);
                    break;
                case NameChangeOperation.Change:
                    OnChangeRecord?.Invoke(this, eventArgs.AbstractRecord as T);
                    break;
                case NameChangeOperation.Delete:
                    OnDeleteRecord?.Invoke(this, eventArgs.AbstractRecord as T);
                    break;
            }
        }

        #endregion

        public ClassTable() : base()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public ClassTable(ref GrozaSDbTransmission.GrozaSDbTransmissionClient clientServiceDB, int id) : base(clientServiceDB, id)
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        protected bool ValidConnection()
        {
            if (ClientServiceDB == null)
            {
                //добавить ф-цию на стороне сервера Ping()
                // ErrorCallbackClient(this, Errors.EnumClientError.NoConnection, "");
                return false;
            }
            return true;
        }


        public virtual void Add(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;

                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add, Record = record.ConvertToProto(Name) };
                var result = ClientServiceDB.ChangeRecord(message, CreateMetadata());
                return;
            }
            catch (RpcException rpcEx)
            {
                throw new ExceptionClient(rpcEx.Message);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task AddAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;

                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add, Record = record.ConvertToProto(Name) };
                var result = await ClientServiceDB.ChangeRecordAsync(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void AddRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB.ChangeRange(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task AddRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = await ClientServiceDB.ChangeRangeAsync(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void RemoveRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB.ChangeRange(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task RemoveRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = await ClientServiceDB.ChangeRangeAsync(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Change(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change, Record = record.ConvertToProto(Name) };

                var result = ClientServiceDB.ChangeRecord(message, CreateMetadata());
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task ChangeAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change, Record = record.ConvertToProto(Name) };
                var result = await ClientServiceDB.ChangeRecordAsync(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void ChangeRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB.ChangeRangeAsync(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public virtual async Task ChangeRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = await ClientServiceDB.ChangeRangeAsync(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Clear()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto()};
                var result = ClientServiceDB.ClearTable(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task CLearAsync()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto() };
                var result = await ClientServiceDB.ClearTableAsync(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Delete(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete, Record = record.ConvertToProto(Name) };
                var result = ClientServiceDB.ChangeRecord(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task DeleteAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete, Record = record.ConvertToProto(Name) };
                var result = await ClientServiceDB.ChangeRecordAsync(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual List<V> Load<V>() where V : AbstractCommonTable
        {
            ClassDataCommon Data = new ClassDataCommon();
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto()};
                Data = ClientServiceDB.LoadTable(message, CreateMetadata()).Records.ConvertToDBModel(Name);
                return Data.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task<List<V>> LoadAsync<V>() where V : AbstractCommonTable
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto() };
                var temp = await ClientServiceDB.LoadTableAsync(message, CreateMetadata());
                var result = temp.Records.ConvertToDBModel(Name);
                return result.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            //catch (FaultException<InheritorsException.ExceptionWCF> except)
            //{
            //    throw new ExceptionDatabase(except.Detail);
            //}
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }





        protected Metadata CreateMetadata()
        {
            return new Metadata() { new Metadata.Entry("clientid", Id.ToString()) };
        }

    }
}

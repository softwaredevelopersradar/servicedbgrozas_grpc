﻿using System;
using GrozaSDbPackage;

namespace GrpcDbClientLib
{
    internal class ClassTable : IDisposable
    {
        public static GrozaSDbTransmission.GrozaSDbTransmissionClient ClientServiceDB { get; internal set; } = null;

        public static int Id { get; internal set; } = 0;

        public ClassTable(GrozaSDbTransmission.GrozaSDbTransmissionClient clientServiceDB, int ID)
        {
            if (ClientServiceDB != null)
                return;

            ClientServiceDB = clientServiceDB;
            Id = ID;
        }

        public ClassTable()
        { }

        public void Dispose()
        {
            if (ClientServiceDB == null)
                return;

            //ClientServiceDB.Abort();
            ClientServiceDB = null;
        }
    }
}

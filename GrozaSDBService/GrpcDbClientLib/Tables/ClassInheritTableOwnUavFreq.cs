﻿using System;
using GrozaSModelsDBLib;
using GrozaSDbPackage;
using ProtoGSLib;

namespace GrpcDbClientLib
{
    class ClassInheritTableOwnUavFreq : ClassTable<TableOwnUavFreqSignal>, IDependentOwnUavFreq
    {
        public ClassInheritTableOwnUavFreq(ref GrozaSDbTransmission.GrozaSDbTransmissionClient clientServiceDB, int id) : base(ref clientServiceDB, id)
        { }

        public ClassInheritTableOwnUavFreq() : base()
        {
        }

        public TableOwnUavFreqSignal LoadByFilter(int ownUavFreqId)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }

                var message = new OwnUAVFreqFilterMessage() { OwnUAVFreqId = ownUavFreqId };
                var data = ClientServiceDB.LoadByFilterOwnUAVFreq(message, CreateMetadata()).ConvertToDBModel();
                return data;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }
        


        public void ClearByFilter(int ownUavFreqId)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }

                var message = new OwnUAVFreqFilterMessage() { OwnUAVFreqId = ownUavFreqId };
                var data = ClientServiceDB.ClearByFilterOwnUAVFreq(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }
    }
}

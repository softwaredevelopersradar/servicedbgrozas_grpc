﻿using System;
using System.Collections.Generic;
using GrozaSModelsDBLib;
using System.Threading.Tasks;
using GrozaSDbPackage;
using ProtoGSLib;

namespace GrpcDbClientLib
{
    internal class ClassInheritTableAsp<T> : ClassTable<T>, IDependentAsp where T : AbstractDependentASP
       
    {
        public ClassInheritTableAsp(ref GrozaSDbTransmission.GrozaSDbTransmissionClient clientServiceDB, int id) : base(ref clientServiceDB, id)
        { }

        public ClassInheritTableAsp() : base()
        {
        }

        public List<V> LoadByFilter<V>(int NumberASP) where V : AbstractDependentASP
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }

                var message = new AspFilterMessage() { Table = Name.ConvertToProto(), NumberASP = NumberASP };
                var data = ClientServiceDB.LoadByFilterAsp(message, CreateMetadata()).Records.ConvertToDBModel(Name);
                return data.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public async Task<List<V>> LoadByFilterAsync<V>(int NumberASP) where V : AbstractDependentASP
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var message = new AspFilterMessage() { Table = Name.ConvertToProto(), NumberASP = NumberASP };
                var data = await ClientServiceDB.LoadByFilterAspAsync(message, CreateMetadata());
                var result = data.Records.ConvertToDBModel(Name);
                return result.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public void ClearByFilter(int NumberASP)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }

                var message = new AspFilterMessage() { Table = Name.ConvertToProto(), NumberASP = NumberASP };
                var data = ClientServiceDB.ClearByFilterAsp(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }
    }
}

﻿using InheritorsEventArgs;
using System;

namespace GrpcDbClientLib
{
    public interface ITableUpdate<T> where T : GrozaSModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}

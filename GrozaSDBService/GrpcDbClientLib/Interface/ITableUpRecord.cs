﻿using System;

namespace GrpcDbClientLib
{
    public interface ITableUpRecord<T> where T : GrozaSModelsDBLib.AbstractCommonTable
    {
        event EventHandler<T> OnAddRecord;

        event EventHandler<T> OnDeleteRecord;

        event EventHandler<T> OnChangeRecord;
    }
}

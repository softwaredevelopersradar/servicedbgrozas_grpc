﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GrpcDbClientLib
{
    public interface IDependentAsp
    {
        List<T> LoadByFilter<T>(int NumberASP) where T : GrozaSModelsDBLib.AbstractDependentASP;
        Task<List<T>> LoadByFilterAsync<T>(int NumberASP) where T : GrozaSModelsDBLib.AbstractDependentASP;
        void ClearByFilter(int NumberASP);
    }
}

﻿using InheritorsEventArgs;
using System;

namespace GrpcDbClientLib
{
    public interface ITableAddRange<T> where T : GrozaSModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrpcDbClientLib
{
    internal interface IClickUpData
    {
        void ClickUpTable(GrozaSModelsDBLib.ClassDataCommon dataCommon);
    }
}

﻿using GrozaSModelsDBLib;

namespace GrpcDbClientLib
{
    public interface IDependentOwnUavFreq
    {
        TableOwnUavFreqSignal LoadByFilter(int ownUavFreqId);
        void ClearByFilter(int ownUavFreqId);
    }
}

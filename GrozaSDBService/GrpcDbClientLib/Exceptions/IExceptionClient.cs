﻿using Errors;

namespace GrpcDbClientLib
{
    public interface IExceptionClient
    {
        EnumClientError Error { get; }

        string Message { get; }
    }
}

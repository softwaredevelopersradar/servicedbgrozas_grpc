﻿using Errors;

namespace InheritorsException
{
    public interface IExceptionDb
    {
        EnumDBError Error { get; }

        string Message { get; }
    }
}

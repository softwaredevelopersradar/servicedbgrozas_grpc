﻿using System;
using GrozaSModelsDBLib;
using GrozaSDbPackage;
using Google.Protobuf.WellKnownTypes;
using System.Collections.Generic;
using Mapster;
using System.Linq;
using Google.Protobuf;

namespace ProtoGSLib
{
    public static class TypesConverter
    {
        #region Enums

        public static GrozaSModelsDBLib.NameTable ConvertToDBModel(this GrozaSDbPackage.EnumTables args) =>
            (GrozaSModelsDBLib.NameTable)args;
        public static GrozaSDbPackage.EnumTables ConvertToProto(this GrozaSModelsDBLib.NameTable args) =>
            (GrozaSDbPackage.EnumTables)args;


        public static GrozaSModelsDBLib.NameChangeOperation ConvertToDBModel(this GrozaSDbPackage.EnumChangeRecordOperation args) =>
            (GrozaSModelsDBLib.NameChangeOperation)args;
        public static GrozaSDbPackage.EnumChangeRecordOperation ConvertToProto(this GrozaSModelsDBLib.NameChangeOperation args) =>
            (GrozaSDbPackage.EnumChangeRecordOperation)args;

        #endregion

        //private Dictionary<>


        #region Models

        public static GrozaSModelsDBLib.TableJammerStation ConvertToDBModel(this GrozaSDbPackage.JammerMessage args)
        {
            return args.Adapt<TableJammerStation>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.JammerMessage ConvertToProto(this GrozaSModelsDBLib.TableJammerStation args)
        {
            return args.Adapt<JammerMessage>(typeAdapterConfig);
        }


        public static GrozaSModelsDBLib.Coord ConvertToDBModel(this GrozaSDbPackage.CoordMessage args)
        {
            return args.Adapt<Coord>();
        }
        public static GrozaSDbPackage.CoordMessage ConvertToProto(this GrozaSModelsDBLib.Coord args)
        {
            return args.Adapt<CoordMessage>();
        }


        public static GrozaSModelsDBLib.FreqRanges ConvertToDBModel(this GrozaSDbPackage.FreqRangeMessage args)
        {
            return args.Adapt<FreqRanges>();
        }
        public static GrozaSDbPackage.FreqRangeMessage ConvertToProto(this GrozaSModelsDBLib.FreqRanges args)
        {
            return args.Adapt<FreqRangeMessage>();
        }


        public static GrozaSModelsDBLib.TableSectorsRecon ConvertToDBModel(this GrozaSDbPackage.SectorMessage args)
        {
            return args.Adapt<TableSectorsRecon>();
        }
        public static GrozaSDbPackage.SectorMessage ConvertToProto(this GrozaSModelsDBLib.TableSectorsRecon args)
        {
            return args.Adapt<SectorMessage>();
        }

        public static GrozaSModelsDBLib.TableCuirasseMPoints ConvertToDBModel(this GrozaSDbPackage.CuirasseMMessage args)
        {
            return args.Adapt<TableCuirasseMPoints>();
        }
        public static GrozaSDbPackage.CuirasseMMessage ConvertToProto(this GrozaSModelsDBLib.TableCuirasseMPoints args)
        {
            return args.Adapt<CuirasseMMessage>();
        }

        public static GrozaSModelsDBLib.TableAeroscope ConvertToDBModel(this GrozaSDbPackage.AeroscopeMessage args)
        {
            return args.Adapt<TableAeroscope>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.AeroscopeMessage ConvertToProto(this GrozaSModelsDBLib.TableAeroscope args)
        {
            return args.Adapt<AeroscopeMessage>(typeAdapterConfig);
        }
        public static GrozaSModelsDBLib.TableAeroscopeTrajectory ConvertToDBModel(this GrozaSDbPackage.AeroscopeTrajectoryMessage args)
        {
            return args.Adapt<TableAeroscopeTrajectory>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.AeroscopeTrajectoryMessage ConvertToProto(this GrozaSModelsDBLib.TableAeroscopeTrajectory args)
        {
            return args.Adapt<AeroscopeTrajectoryMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TableJamBearing ConvertToDBModel(this GrozaSDbPackage.BearingMessage args)
        {
            return args.Adapt<TableJamBearing>();
        }
        public static GrozaSDbPackage.BearingMessage ConvertToProto(this GrozaSModelsDBLib.TableJamBearing args)
        {
            return args.Adapt<BearingMessage>();
        }

        public static GrozaSModelsDBLib.TableTrack ConvertToDBModel(this GrozaSDbPackage.TrackMessage args)
        {
        //    TypeAdapterConfig<TableTrack, TrackMessage>.NewConfig().Fork(config => config.ForType<RepeatedField<BearingMessage>, ObservableCollection<TableJamBearing>>()
        //    .MapToTargetWith((src, dest) => new ObservableCollection<TableJamBearing>(src.ToList().Adapt<List<TableJamBearing>>())));
            //return args.Adapt<TableTrack>(typeAdapterConfig);
            return args.Adapt<TableTrack>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.TrackMessage ConvertToProto(this GrozaSModelsDBLib.TableTrack args)
        {
            //TypeAdapterConfig<TrackMessage, TableTrack>.NewConfig().PreserveReference(true);
            //TypeAdapterConfig<TableTrack, TrackMessage>.NewConfig().PreserveReference(true);
            //TypeAdapterConfig<BearingMessage, TableJamBearing>.NewConfig().PreserveReference(true);
            //TypeAdapterConfig<TableJamBearing, BearingMessage>.NewConfig().PreserveReference(true);

            //Func<ObservableCollection<TableJamBearing>, RepeatedField<BearingMessage>, RepeatedField<BearingMessage>> cp = (src, dest) =>
            //{
            //    RepeatedField<BearingMessage> temp = new RepeatedField<BearingMessage>();
            //    temp.AddRange(src.Adapt<List<BearingMessage>>());
            //    return temp;
            //};

            //Expression< Func< ObservableCollection<TableJamBearing>, RepeatedField<BearingMessage>, RepeatedField<BearingMessage>>> gg = (src, dest) => cp(src, dest);

            //TypeAdapterConfig<TrackMessage, TableTrack>.NewConfig().Fork(config => config.ForType<ObservableCollection<TableJamBearing>, RepeatedField<BearingMessage>>()
            //.MapToTargetWith(gg));
            var final = args.Adapt<TrackMessage>(typeAdapterConfig);
            final.Bearing.AddRange(args.Bearing.Select(x => ConvertToProto(x)));
            return final;
        }

        public static GrozaSModelsDBLib.TableSource ConvertToDBModel(this GrozaSDbPackage.SourceMessage args)
        {
            return args.Adapt<TableSource>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.SourceMessage ConvertToProto(this GrozaSModelsDBLib.TableSource args)
        {
            var final = args.Adapt<SourceMessage>(typeAdapterConfig);
            final.Track.AddRange(args.Track.Select(x => ConvertToProto(x)));
            return final;
        }


        private static readonly TypeAdapterConfig typeAdapterConfig = GetTypeAdapterConfig();

        private static TypeAdapterConfig GetTypeAdapterConfig()
        {
            var config = new TypeAdapterConfig();
            config.NewConfig<Timestamp, DateTime>().MapWith(src=>src.ToDateTime() );
            config.NewConfig<DateTime, Timestamp>().MapWith(src => DateTime.SpecifyKind(src, DateTimeKind.Utc).ToTimestamp());

            //config.NewConfig<OperatingFrequency, OperatingFrequencyMessage>().MapWith(src => src.Adapt<OperatingFrequencyMessage>());
            //config.NewConfig<OperatingFrequencyMessage, OperatingFrequency>().MapWith(src => src.Adapt<OperatingFrequency>());

            config.NewConfig<byte[], ByteString>().MapWith(src => ByteString.CopyFrom(src));
            config.NewConfig<ByteString, byte[]>().MapWith(src => src.ToArray());
            config.NewConfig<TablePattern, PatternMessage>().IgnoreNullValues(true);
            config.NewConfig<PatternMessage, TablePattern>().IgnoreNullValues(true);
            config.NewConfig<TableOwnUavFreqSignal, OwnUAVFreqSignalMessage>().IgnoreNullValues(true);
            config.NewConfig<OwnUAVFreqSignalMessage, TableOwnUavFreqSignal>().IgnoreNullValues(true);
           
            //config.NewConfig<RepeatedField<T>, ObservableCollection<T>>().MapWith(src => {
            //    var targetList = new ObservableCollection<T>();
            //    foreach (var t in args.ListSubscribers)
            //    {
            //        targetList.Add(t.ConvertToDBModel());
            //    }
            //    src.ToDateTime()});
            //config.NewConfig<DateTime, Timestamp>().MapWith(src => Timestamp.FromDateTime(src.ToUniversalTime()));
            //config.Default.AddDestinationTransform((IReadOnlyList<ChildDto> list) => list ?? new List<ChildDto>());

            //config.NewConfig<TableTrack, TrackMessage>();
            //config.NewConfig<TableJamBearing, BearingMessage>();
            //config.NewConfig<BearingMessage, TableJamBearing>();
            //TypeAdapterConfig.GlobalSettings.Default.AddDestinationTransform((RepeatedField<TableJamBearing> list) => new ObservableCollection<TableJamBearing>(list.ToList()));
            return config;
        }

        public static GrozaSModelsDBLib.GlobalProperties ConvertToDBModel(this GrozaSDbPackage.GlobalSettingsMessage args)
        {
            return args.Adapt<GlobalProperties>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.GlobalSettingsMessage ConvertToProto(this GrozaSModelsDBLib.GlobalProperties args)
        {
            return args.Adapt<GlobalSettingsMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TableSuppressSource ConvertToDBModel(this GrozaSDbPackage.SuppressSourceMessage args)
        {
            return args.Adapt<TableSuppressSource>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.SuppressSourceMessage ConvertToProto(this GrozaSModelsDBLib.TableSuppressSource args)
        {
            return args.Adapt<SuppressSourceMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TableSuppressGnss ConvertToDBModel(this GrozaSDbPackage.SuppressGnssMessage args)
        {
            return args.Adapt<TableSuppressGnss>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.SuppressGnssMessage ConvertToProto(this GrozaSModelsDBLib.TableSuppressGnss args)
        {
            return args.Adapt<SuppressGnssMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TablePattern ConvertToDBModel(this GrozaSDbPackage.PatternMessage args)
        {
            //var final = args.Adapt<TablePattern>(typeAdapterConfig);
            //final.FrequencyList = new ObservableCollection<OperatingFrequency>(args.FrequencyList.Select(x => x.Adapt<OperatingFrequency>()));
            //return final;
            return args.Adapt<TablePattern>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.PatternMessage ConvertToProto(this GrozaSModelsDBLib.TablePattern args)
        {
            var final = args.Adapt<PatternMessage>(typeAdapterConfig);
            final.FrequencyList.AddRange(args.FrequencyList.Select(x => x.Adapt<OperatingFrequencyMessage>()));
            return final;
            //return args.Adapt<PatternMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TableOwnUAVFreq ConvertToDBModel(this GrozaSDbPackage.OwnUAVFreqMessage args)
        {
            return args.Adapt<TableOwnUAVFreq>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.OwnUAVFreqMessage ConvertToProto(this GrozaSModelsDBLib.TableOwnUAVFreq args)
        {
            return args.Adapt<OwnUAVFreqMessage>(typeAdapterConfig);
        }

        public static GrozaSModelsDBLib.TableOwnUAV ConvertToDBModel(this GrozaSDbPackage.OwnUAVMessage args)
        {
            return args.Adapt<TableOwnUAV>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.OwnUAVMessage ConvertToProto(this GrozaSModelsDBLib.TableOwnUAV args)
        {
            var final = args.Adapt<OwnUAVMessage>(typeAdapterConfig);
            final.Frequencies.AddRange(args.Frequencies.Select(x => ConvertToProto(x)));
            return final;
        }

        public static GrozaSModelsDBLib.TableOwnUavFreqSignal ConvertToDBModel(this GrozaSDbPackage.OwnUAVFreqSignalMessage args)
        {
            return args.Adapt<TableOwnUavFreqSignal>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.OwnUAVFreqSignalMessage ConvertToProto(this GrozaSModelsDBLib.TableOwnUavFreqSignal args)
        {
            return args.Adapt<OwnUAVFreqSignalMessage>(typeAdapterConfig);
        }


        public static GrozaSModelsDBLib.TableOperatorGun ConvertToDBModel(this GrozaSDbPackage.OperatorGunMessage args)
        {
            return args.Adapt<TableOperatorGun>(typeAdapterConfig);
        }
        public static GrozaSDbPackage.OperatorGunMessage ConvertToProto(this GrozaSModelsDBLib.TableOperatorGun args)
        {
            return args.Adapt<OperatorGunMessage>(typeAdapterConfig);
        }

        #endregion




        #region General

        public static GrozaSModelsDBLib.AbstractCommonTable ConvertToDBModel(this Google.Protobuf.WellKnownTypes.Any args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableJammerStation:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.JammerMessage>());
                case NameTable.TableFreqForbidden:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.FreqRangeMessage>()).ToFreqForbidden();
                case NameTable.TableFreqRangesRecon:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.FreqRangeMessage>()).ToRangesRecon();
                case NameTable.TableFreqKnown:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.FreqRangeMessage>()).ToFreqKnown();
                case NameTable.TableSectorsRecon:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.SectorMessage>());
                case NameTable.TableCuirasseMPoints:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.CuirasseMMessage>());
                case NameTable.TablePattern:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.PatternMessage>());
                case NameTable.TableOwnUAV:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.OwnUAVMessage>());
                case NameTable.TableOwnUAVFreq:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.OwnUAVFreqMessage>());
                case NameTable.TableOwnUAVFreqSignal:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.OwnUAVFreqSignalMessage>());
                case NameTable.TableSuppressSource:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.SuppressSourceMessage>());
                case NameTable.TableSuppressGnss:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.SuppressGnssMessage>());
                case NameTable.GlobalProperties:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.GlobalSettingsMessage>());                
                case NameTable.TableSource:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.SourceMessage>());
                case NameTable.TableTrack:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.TrackMessage>());
                case NameTable.TableJamBearing:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.BearingMessage>());
                case NameTable.TableAeroscope:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.AeroscopeMessage>());
                case NameTable.TableAeroscopeTrajectory:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.AeroscopeTrajectoryMessage>());
                case NameTable.TableOperatorGun:
                    return ConvertToDBModel(args.Unpack<GrozaSDbPackage.OperatorGunMessage>());
                default:
                    return null;

            }
        }
        public static Google.Protobuf.WellKnownTypes.Any ConvertToProto(this GrozaSModelsDBLib.AbstractCommonTable args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableJammerStation:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableJammerStation));
                case NameTable.TableFreqForbidden:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqForbidden));
                case NameTable.TableFreqRangesRecon:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqRangesRecon));
                case NameTable.TableFreqKnown:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqKnown));
                case NameTable.TableSectorsRecon:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSectorsRecon));
                case NameTable.TableCuirasseMPoints:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableCuirasseMPoints));
                case NameTable.TablePattern:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TablePattern));
                case NameTable.TableOwnUAV:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableOwnUAV));
                case NameTable.TableOwnUAVFreq:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableOwnUAVFreq));
                case NameTable.TableOwnUAVFreqSignal:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableOwnUavFreqSignal));
                case NameTable.TableSuppressSource:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSuppressSource));
                case NameTable.TableSuppressGnss:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSuppressGnss));
                case NameTable.GlobalProperties:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as GlobalProperties));
                case NameTable.TableSource:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSource));
                case NameTable.TableTrack:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableTrack));
                case NameTable.TableJamBearing:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableJamBearing));
                case NameTable.TableAeroscope:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableAeroscope));
                case NameTable.TableAeroscopeTrajectory:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableAeroscopeTrajectory));
                case NameTable.TableOperatorGun:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableOperatorGun));
                default:
                    return null;
            }
        }



        public static GrozaSModelsDBLib.ClassDataCommon ConvertToDBModel(this Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> args, NameTable nameTable)
        {
            var result = new List<AbstractCommonTable>();

            foreach (var t in args)
            {
                result.Add(t.ConvertToDBModel(nameTable));
            }
            return ClassDataCommon.ConvertToListAbstractCommonTable(result);
        }

        public static Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> ConvertToProto(this GrozaSModelsDBLib.ClassDataCommon args, NameTable nameTable)
        {
            var result = new Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any>();

            foreach (var t in args.ListRecords)
            {
                result.Add(t.ConvertToProto(nameTable));
            }
            return result;
        }

        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Траектория А БПЛА
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [CategoryOrder("Прочее", 4)]
    [InfoTable(NameTable.TableAeroscopeTrajectory)]
    public class TableAeroscopeTrajectory : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Num))]
        public int Num { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Elevation))]
        public float Elevation { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Time))]
        public DateTime Time { get; set; }

        [DataMember]
        public string SerialNumber { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Roll))]
        public float Roll { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Pitch))]
        public float Pitch { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Yaw))]
        public float Yaw { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_up))]
        public float V_up { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_east))]
        public float V_east { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_north))]
        public float V_north { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            var rec = (TableAeroscopeTrajectory)record;
            Num = rec.Num;
            Coordinates.Update(rec.Coordinates);
            Elevation = rec.Elevation;
            Time = rec.Time;
            Roll = rec.Roll;
            Pitch = rec.Pitch;
            Yaw = rec.Yaw;
            V_up = rec.V_up;
            V_east = rec.V_east;
            V_north = rec.V_north;
        }
    }
}

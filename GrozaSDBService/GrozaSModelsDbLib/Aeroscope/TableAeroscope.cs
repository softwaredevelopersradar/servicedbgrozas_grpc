﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Аэроскоп
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableAeroscope)]
    public class TableAeroscope : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        public override int Id { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(SerialNumber))]
        public string SerialNumber { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(UUIDLength))]
        public double UUIDLength { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(UUID))]
        public string UUID { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(HomeLatitude))]
        public double HomeLatitude { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(HomeLongitude))]
        public double HomeLongitude { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Type))]
        public string Type { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }




        public override object[] GetKey()
        {
            return new object[] { SerialNumber };
        }

        public override void Update(AbstractCommonTable record)
        {
            var rec = (TableAeroscope)record;
            Id = rec.Id;
            UUIDLength = rec.UUIDLength;
            UUID = rec.UUID;
            HomeLatitude = rec.HomeLatitude;
            HomeLongitude = rec.HomeLongitude;
            Type = rec.Type;
            IsActive = rec.IsActive;
        }

        public TableAeroscope Clone()
        {
            return new TableAeroscope()
            {
                Id = this.Id,
                SerialNumber = this.SerialNumber,
                UUIDLength = this.UUIDLength,
                UUID = this.UUID,
                HomeLatitude = this.HomeLatitude,
                HomeLongitude = this.HomeLongitude,
                Type = this.Type,
                IsActive = this.IsActive

            };
        }
    }
}

﻿using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Шаблон
    /// </summary>
    [DataContract]
    [KnownType(typeof(OperatingFrequency))]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TablePattern)]
    public class TablePattern : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public string Name { get; set; } = string.Empty;

        [DataMember]
        public byte View { get; set; }

        [DataMember]
        public ObservableCollection<OperatingFrequency> FrequencyList { get; set; } = new ObservableCollection<OperatingFrequency>();

        [DataMember]
        public byte[] ImageByte { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Name = ((TablePattern)record).Name;
            View = ((TablePattern)record).View;
            FrequencyList = new ObservableCollection<OperatingFrequency>(((TablePattern)record).FrequencyList);
            ImageByte = ((TablePattern)record).ImageByte;
        }
    }
}

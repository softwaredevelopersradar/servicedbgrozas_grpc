﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public class ParamCmp : INotifyPropertyChanged
    {
        private float _angle = 0;

        [DataMember]
        [NotifyParentProperty(true)]
        public float Angle
        {
            get => _angle;
            set
            {
                if (_angle == value) return;
                _angle = value;
                OnPropertyChanged();
            }
        }
        

        public ParamCmp() { }

        public ParamCmp(float angle)
        {
            Angle = angle;
        }



        #region Model Methods

        public bool EqualTo(ParamCmp model)
        {
            return Angle == model.Angle;
        }

        public ParamCmp Clone() => new ParamCmp(Angle);

        public void Update(ParamCmp model)
        {
            Angle = model.Angle;
        }

        #endregion


        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}

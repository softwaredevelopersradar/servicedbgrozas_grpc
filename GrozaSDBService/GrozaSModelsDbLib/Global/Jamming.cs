﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public class Jamming : INotifyPropertyChanged
    {
        public Jamming() { }
        public Jamming(short timeRadiat, short power100_500, short power500_2500, short power2500_6000, int distanceJamming, float sector)
        {
            TimeRadiat = timeRadiat;
            Power100_500 = power100_500;
            Power500_2500 = power500_2500;
            Power2500_6000 = power2500_6000;
            DistanceJamming = distanceJamming;
            Sector = sector;
        }


        #region IModelMethods
        public Jamming Clone() => new Jamming(TimeRadiat, Power100_500, Power500_2500, Power2500_6000, DistanceJamming, Sector);

        public bool EqualTo(Jamming model)
        {
            return TimeRadiat == model.TimeRadiat && Power100_500 == model.Power100_500
                && Power500_2500 == model.Power500_2500 && Power2500_6000 == model.Power2500_6000
                && DistanceJamming == model.DistanceJamming && Sector == model.Sector;
        }

        public void Update(Jamming model)
        {
            TimeRadiat = model.TimeRadiat;
            Power100_500 = model.Power100_500;
            Power500_2500 = model.Power500_2500;
            Power2500_6000 = model.Power2500_6000;
            DistanceJamming = model.DistanceJamming;
            Sector = model.Sector;
        }
        #endregion



        private short _timeRadiate = 3;
        private short _power100_500 = 125;
        private short _power500_2500 = 50;
        private short _power2500_6000 = 10;
        private int _distanceJamming = 50_000;
        private float _sector = 60;

        [DataMember]
        [NotifyParentProperty(true)]
        public short TimeRadiat
        {
            get => _timeRadiate;
            set
            {
                if (_timeRadiate == value) return;
                _timeRadiate = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short Power100_500
        {
            get => _power100_500;
            set
            {
                if (_power100_500 == value) return;
                _power100_500 = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short Power500_2500
        {
            get => _power500_2500;
            set
            {
                if (_power500_2500 == value) return;
                _power500_2500 = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public short Power2500_6000
        {
            get => _power2500_6000;
            set
            {
                if (_power2500_6000 == value) return;
                _power2500_6000 = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public int DistanceJamming
        {
            get { return _distanceJamming; }
            set
            {
                if (value == _distanceJamming)
                    return;
                _distanceJamming = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float Sector
        {
            get { return _sector; }
            set
            {
                if (value == _sector)
                    return;
                _sector = value;
                OnPropertyChanged();
            }
        }


        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}

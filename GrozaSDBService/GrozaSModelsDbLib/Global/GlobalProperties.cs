﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalProperties)]
    #region CategoryOrder
    [CategoryOrder("Common", 1)]
    [CategoryOrder("RadioIntelegence", 2)]
    [CategoryOrder("Jamming", 3)]
    #endregion
    
    public class GlobalProperties : AbstractCommonTable, INotifyPropertyChanged
    {

        #region CategotyName
        public const string GnssCategory = "GNSS";
        public const string CmpRXCategory = "CmpRX";
        public const string CmpTXCategory = "CmpTX";
        public const string OemCategory = "OEM";
        public const string RadioIntelCategory = "RadioIntelegence";
        public const string JammingCategory = "Jamming";
        public const string SpoofingCategory = "Spoofing";
        #endregion


        public GlobalProperties()
        {
            //Jamming = new Jamming();
            //RadioIntelegence = new RadioIntelegence();
            //Gnss = new Coord();
            //CmpRX = new ParamCmp();
            //CmpTX = new ParamCmp();
            //Spoofing = new Coord();
            //Oem = new ParamOEM();

            //Gnss.PropertyChanged += PropertyChanged;
            //Spoofing.PropertyChanged += PropertyChanged;
            //CmpRX.PropertyChanged += PropertyChanged;
            //CmpTX.PropertyChanged += PropertyChanged;
            //Oem.PropertyChanged += PropertyChanged;
            //Jamming.PropertyChanged += PropertyChanged;
            //RadioIntelegence.PropertyChanged += PropertyChanged;
        }


        public GlobalProperties(Coord gnss, ParamCmp cmpRx, ParamCmp cmpTx, ParamOEM oem,
            RadioIntelegence radioIntelegence, Jamming jamming, Coord spoofing, int id = 0)
        {
            Id = id;

            Gnss = gnss;
            CmpRX = cmpRx;
            CmpTX = cmpTx;
            Oem = oem;

            Jamming = jamming;
            RadioIntelegence = radioIntelegence;

            Spoofing = spoofing;

            //Gnss.PropertyChanged += Gnss_PropertyChanged; /*PropertyChanged;*/
            //Spoofing.PropertyChanged += PropertyChanged;
            //CmpRX.PropertyChanged += PropertyChanged;
            //CmpTX.PropertyChanged += PropertyChanged;
            //Jamming.PropertyChanged += PropertyChanged;
            //Jamming.PropertyChanged += PropertyChanged;
            //RadioIntelegence.PropertyChanged += PropertyChanged;
        }

        private void Gnss_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        private Coord _gnss = new Coord();

        [DataMember]
        [Category(nameof(GnssCategory))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Gnss
        {
            get { return _gnss; }
            set
            {
                if (_gnss == value)
                    return;
                _gnss = value;
                OnPropertyChanged();
            }
        }


        private ParamCmp _сmpRX = new ParamCmp();
        private ParamCmp _сmpTX = new ParamCmp();

        [DataMember]
        [Category(nameof(CmpRXCategory))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamCmp CmpRX
        {
            get { return _сmpRX; }
            set
            {
                if (_сmpRX == value)
                    return;
                _сmpRX = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [Category(nameof(CmpTXCategory))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamCmp CmpTX
        {
            get { return _сmpTX; }
            set
            {
                if (_сmpTX == value)
                    return;
                _сmpTX = value;
                OnPropertyChanged();
            }
        }


        private ParamOEM _oem = new ParamOEM();

        [DataMember]
        [Category(nameof(OemCategory))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamOEM Oem
        {
            get { return _oem; }
            set
            {
                if (_oem == value)
                    return;
                _oem = value;
                OnPropertyChanged();
            }
        }


        private RadioIntelegence _radioIntelegence = new RadioIntelegence();

        [DataMember]
        [Category(nameof(RadioIntelegence))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RadioIntelegence RadioIntelegence
        {
            get { return _radioIntelegence; }
            set
            {
                if (_radioIntelegence == value)
                    return;
                _radioIntelegence = value;
                OnPropertyChanged();
            }
        }


        private Jamming _jamming = new Jamming();

        [DataMember]
        [Category(nameof(Jamming))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Jamming Jamming
        {
            get { return _jamming; }
            set
            {
                if (_jamming == value)
                    return;
                _jamming = value;
                OnPropertyChanged();
            }
        }


        private Coord _spoofing = new Coord();

        [DataMember]
        [Category(nameof(SpoofingCategory))]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Spoofing
        {
            get { return _spoofing; }
            set
            {
                if (_spoofing == value)
                    return;
                _spoofing = value;
                OnPropertyChanged();
            }
        }


        #region Methods
        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (GlobalProperties)record;

            Gnss.Altitude = newRecord.Gnss.Altitude;
            Gnss.Latitude = newRecord.Gnss.Latitude;
            Gnss.Longitude = newRecord.Gnss.Longitude;

            CmpRX.Angle = newRecord.CmpRX.Angle;

            CmpTX.Angle = newRecord.CmpTX.Angle;

            Oem.Distance = newRecord.Oem.Distance;

            RadioIntelegence.Update(newRecord.RadioIntelegence);
            Jamming.Update(newRecord.Jamming);

            Spoofing.Altitude = newRecord.Spoofing.Altitude;
            Spoofing.Latitude = newRecord.Spoofing.Latitude;
            Spoofing.Longitude = newRecord.Spoofing.Longitude;
        }


        public GlobalProperties Clone() => new GlobalProperties(Gnss, CmpRX.Clone(), CmpTX.Clone(), Oem.Clone(), 
            RadioIntelegence.Clone(), Jamming.Clone(), Spoofing, Id);


        public bool EqualTo(GlobalProperties data)
        {
            return Gnss == data.Gnss && CmpRX.EqualTo(data.CmpRX) && CmpTX.EqualTo(data.CmpTX) && Oem.EqualTo(data.Oem)
                && Jamming.EqualTo(data.Jamming) && RadioIntelegence.EqualTo(data.RadioIntelegence)
                && Spoofing == data.Spoofing;
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}

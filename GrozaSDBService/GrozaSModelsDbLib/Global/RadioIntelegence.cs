﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public class RadioIntelegence : INotifyPropertyChanged
    {
        public RadioIntelegence() { }
        public RadioIntelegence(int zoneAttention, int zoneReadiness, int zoneAlarm,
            int distanceIntelegence, float bearingAccuracy, float frequencyAccuracy, float staticAngle,
            float bandAccurancy, float bandLargerError, float bandLessError, TSSB ssb, byte numOfScans, ScanChannels scanChannel, ScanChannels detectionChannel)
        {
            ZoneAttention = zoneAttention;
            ZoneReadiness = zoneReadiness;
            ZoneAlarm = zoneAlarm;
            DistanceIntelegence = distanceIntelegence;
            BearingAccuracy = bearingAccuracy;
            FrequencyAccuracy = frequencyAccuracy;
            StaticAngle = staticAngle;
            BandAccuracy = bandAccurancy;
            BandLargerError = bandLargerError;
            BandLessError = bandLessError;
            SSB = ssb;
            NumOfScans = numOfScans;
            ScanChannel = scanChannel;
            DetectionChannel = detectionChannel;
        }


        #region IModelMethods
        public RadioIntelegence Clone() => new RadioIntelegence(ZoneAttention, ZoneReadiness, ZoneAlarm, DistanceIntelegence, BearingAccuracy, FrequencyAccuracy, StaticAngle, BandAccuracy, BandLargerError, BandLessError, SSB, NumOfScans, ScanChannel, DetectionChannel);

        public bool EqualTo(RadioIntelegence model)
        {
            return ZoneAttention == model.ZoneAttention && ZoneReadiness == model.ZoneReadiness
                && ZoneAlarm == model.ZoneAlarm && DistanceIntelegence == model.DistanceIntelegence
                && BearingAccuracy == model.BearingAccuracy && FrequencyAccuracy == model.FrequencyAccuracy
                && StaticAngle == model.StaticAngle && BandAccuracy == model.BandAccuracy && BandLargerError == model.BandLargerError && BandLessError == model.BandLessError
                && SSB == model.SSB && NumOfScans == model.NumOfScans && ScanChannel == model.ScanChannel && DetectionChannel == model.DetectionChannel;
        }

        public void Update(RadioIntelegence model)
        {
            ZoneAttention = model.ZoneAttention;
            ZoneReadiness = model.ZoneReadiness;
            ZoneAlarm = model.ZoneAlarm;
            DistanceIntelegence = model.DistanceIntelegence;
            BearingAccuracy = model.BearingAccuracy;
            FrequencyAccuracy = model.FrequencyAccuracy;
            StaticAngle = model.StaticAngle;
            BandAccuracy = model.BandAccuracy;
            BandLargerError = model.BandLargerError;
            BandLessError = model.BandLessError;
            SSB = model.SSB;
            NumOfScans = model.NumOfScans;
            ScanChannel = model.ScanChannel;
            DetectionChannel = model.DetectionChannel;
        }
        #endregion


        //private bool _autoCourse = true;
        //private int _courseAngle = 0;
        private int _zoneAttention = 50000;
        private int _zoneReadiness = 20000;
        private int _zoneAlarm = 10000;
        private int _distanceIntelegence = 50_000;
        private float _bearingAccuracy = 22.5f;
        private float _frequencyAccuracy = 5;
        private float _staticAngle = -1;
        private float _bandAccuracy = 2;
        private float _bandLargerError = 10.0f;
        private float _bandLessError = 10.0f;
        private TSSB _ssb = TSSB.Band160;
        private byte _numOfScans = 6;
        private ScanChannels _scanChannel = ScanChannels.Ch_1;
        private ScanChannels _detectionChannel = ScanChannels.Ch_1;



        [DataMember]
        [NotifyParentProperty(true)]
        public int ZoneAttention
        {
            get { return _zoneAttention; }
            set
            {
                if (value == _zoneAttention)
                    return;
                _zoneAttention = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public int ZoneReadiness
        {
            get { return _zoneReadiness; }
            set
            {
                if (value == _zoneReadiness)
                    return;
                _zoneReadiness = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public int ZoneAlarm
        {
            get { return _zoneAlarm; }
            set
            {
                if (value == _zoneAlarm)
                    return;
                _zoneAlarm = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public int DistanceIntelegence
        {
            get { return _distanceIntelegence; }
            set
            {
                if (value == _distanceIntelegence)
                    return;
                _distanceIntelegence = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float BearingAccuracy
        {
            get { return _bearingAccuracy; }
            set
            {
                if (value == _bearingAccuracy)
                    return;
                _bearingAccuracy = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float FrequencyAccuracy
        {
            get { return _frequencyAccuracy; }
            set
            {
                if (value == _frequencyAccuracy)
                    return;
                _frequencyAccuracy = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float StaticAngle
        {
            get { return _staticAngle; }
            set
            {
                if (value == _staticAngle)
                    return;
                _staticAngle = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float BandAccuracy
        {
            get { return _bandAccuracy; }
            set
            {
                if (value == _bandAccuracy)
                    return;
                _bandAccuracy = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float BandLargerError
        {
            get { return _bandLargerError; }
            set
            {
                if (value == _bandLargerError)
                    return;
                _bandLargerError = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float BandLessError
        {
            get { return _bandLessError; }
            set
            {
                if (value == _bandLessError)
                    return;
                _bandLessError = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public TSSB SSB
        {
            get { return _ssb; }
            set
            {
                if (value == _ssb)
                    return;
                _ssb = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public byte NumOfScans
        {
            get { return _numOfScans; }
            set
            {
                if (value == _numOfScans)
                    return;
                _numOfScans = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public ScanChannels ScanChannel
        {
            get { return _scanChannel; }
            set
            {
                if (value == _scanChannel)
                    return;
                _scanChannel = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public ScanChannels DetectionChannel
        {
            get { return _detectionChannel; }
            set
            {
                if (value == _detectionChannel)
                    return;
                _detectionChannel = value;
                OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}

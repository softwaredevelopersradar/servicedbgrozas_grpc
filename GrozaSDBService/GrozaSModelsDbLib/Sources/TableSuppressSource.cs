﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// ИРИ на подавление
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableSuppressSource)]
    public class TableSuppressSource : AbstractCommonTable, INotifyPropertyChanged
    {

        private int _id;
        [DataMember]
        [Browsable(false)]
        public override int Id
        {
            get => _id;
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }


        private double _frequencyMHz;
        [DataMember]
        [PropertyOrder(1)]
        [DisplayName(nameof(FrequencyMHz))]
        [Category("Common")]
        public double FrequencyMHz
        {
            get => _frequencyMHz;
            set
            {
                if (_frequencyMHz == value) return;
                _frequencyMHz = value;
                OnPropertyChanged();
            }
        }


        private float _bandMHz;
        [DataMember]
        [PropertyOrder(2)]
        [DisplayName(nameof(BandMHz))]
        [Category("Common")]
        public float BandMHz
        {
            get => _bandMHz;
            set
            {
                if (_bandMHz == value) return;
                _bandMHz = value;
                OnPropertyChanged();
            }
        }


        private byte _type;
        [DataMember]
        [PropertyOrder(3)]
        [DisplayName(nameof(Type))]
        [Category("Common")]
        public byte Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }


        private short _threshold;
        [DataMember]
        [Category()]
        [Browsable(false)]
        public short Threshold
        {
            get => _threshold;
            set
            {
                if (_threshold == value) return;
                _threshold = value;
                OnPropertyChanged();
            }
        }


        private short _levelOwn;
        [DataMember]
        [Browsable(false)]
        public short LevelOwn
        {
            get => _levelOwn;
            set
            {
                if (_levelOwn == value) return;
                _levelOwn = value;
                OnPropertyChanged();
            }
        }


        private int _tableSourceId;
        [DataMember]
        [PropertyOrder(4)]
        [Category("Common")]
        [DisplayName(nameof(TableSourceId))]
        public int TableSourceId
        {
            get => _tableSourceId;
            set
            {
                if (_tableSourceId == value) return;
                _tableSourceId = value;
                OnPropertyChanged();
            }
        }


        private TypeInput _inputType= TypeInput.Manual;
        [DataMember]
        [Browsable(false)]
        public TypeInput InputType
        {
            get => _inputType;
            set
            {
                if (_inputType == value) return;
                _inputType = value;
                OnPropertyChanged();
            }
        }


        private Modulation _modulation = Modulation.None;
        [DataMember]
        [PropertyOrder(5)]
        [DisplayName(nameof(Modulation))]
        [Category("Common")]
        public Modulation Modulation
        {
            get => _modulation;
            set
            {
                if (_modulation == value) return;
                _modulation = value;
                OnPropertyChanged();
            }
        }


        private int _deviation;
        [DataMember]
        [PropertyOrder(6)]
        [DisplayName(nameof(Deviation))]
        [Category("Common")]
        public int Deviation
        {
            get => _deviation;
            set
            {
                if (_deviation == value) return;
                _deviation = value;
                OnPropertyChanged();
            }
        }


        private int _scanSpeed;
        [DataMember]
        [PropertyOrder(7)]
        [DisplayName(nameof(ScanSpeed))]
        [Category("Common")]
        public int ScanSpeed
        {
            get => _scanSpeed;
            set
            {
                if (_scanSpeed == value) return;
                _scanSpeed = value;
                OnPropertyChanged();
            }
        }


        private byte _manipulation;
        [DataMember]
        [PropertyOrder(8)]
        [DisplayName(nameof(Manipulation))]
        [Category("Common")]
        public byte Manipulation
        {
            get => _manipulation;
            set
            {
                if (_manipulation == value) return;
                _manipulation = value;
                OnPropertyChanged();
            }
        }





        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSuppressSource)record;

            FrequencyMHz = newRecord.FrequencyMHz;
            BandMHz = newRecord.BandMHz;
            Type = newRecord.Type;
            Threshold = newRecord.Threshold;
            LevelOwn = newRecord.LevelOwn;
            TableSourceId = newRecord.TableSourceId;
            InputType = newRecord.InputType;
            Modulation = newRecord.Modulation;
            Deviation = newRecord.Deviation;
            Manipulation = newRecord.Manipulation;
            ScanSpeed = newRecord.ScanSpeed;
        }

        public TableSuppressSource Clone()
        {
            return new TableSuppressSource()
            {
                Id = Id,
                FrequencyMHz = FrequencyMHz,
                BandMHz = BandMHz,
                Type = Type,
                Threshold = Threshold,
                LevelOwn = LevelOwn,
                TableSourceId = TableSourceId,
                InputType = InputType,
                Modulation = Modulation,
                Deviation = Deviation,
                Manipulation = Manipulation,
                ScanSpeed = ScanSpeed
            };
        }

        public bool EqualTo(TableSuppressSource model)
        {
            return FrequencyMHz == model.FrequencyMHz && BandMHz == model.BandMHz
                && Type == model.Type && Threshold == model.Threshold
                && LevelOwn == model.LevelOwn && TableSourceId == model.TableSourceId
                && Modulation == model.Modulation && Deviation == model.Deviation 
                && Manipulation == model.Manipulation && ScanSpeed == model.ScanSpeed;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}

﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Станция помех
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableCuirasseMPoints)]
    public class TableCuirasseMPoints : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        [Browsable(true)]
        [PropertyOrder(1)]
        public override int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableCuirasseMPoints)record;
            Coordinates.Altitude = newRecord.Coordinates.Altitude;
            Coordinates.Latitude = newRecord.Coordinates.Latitude;
            Coordinates.Longitude = newRecord.Coordinates.Longitude;
            Note = newRecord.Note;
        }


        public TableCuirasseMPoints Clone()
        {
            return new TableCuirasseMPoints
            {
                Id = Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Note = Note
            };
        }
    }
}

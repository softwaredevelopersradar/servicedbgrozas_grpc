﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Запрещенные частоты (ЗЧ)
    /// </summary>

    [DataContract]
    [KnownType(typeof(FreqRanges))]
    [InfoTable(NameTable.TableFreqForbidden)]
    public class TableFreqForbidden : FreqRanges
    {
    }
}

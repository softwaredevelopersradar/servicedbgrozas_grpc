﻿
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using GrozaSModelsDBLib.Interfaces;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Диапазоны частот
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Диапазон", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqKnown))]

    public class FreqRanges : AbstractDependentASP, IFixFreqRanges
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(NumberASP)), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; }  

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(1)]
        [DisplayName(nameof(FreqMinKHz))]
        public double FreqMinKHz { get; set; } 

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(2)]
        [DisplayName(nameof(FreqMaxKHz))]
        public double FreqMaxKHz { get; set; } 

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }


        public FreqRanges Clone()
        {
            return new FreqRanges
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableFreqRangesRecon ToRangesRecon()
        {
            TableFreqRangesRecon table = new TableFreqRangesRecon()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public TableFreqKnown ToFreqKnown()
        {
            TableFreqKnown table = new TableFreqKnown()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public TableFreqForbidden ToFreqForbidden()
        {
            TableFreqForbidden table = new TableFreqForbidden()
            {
                Id = this.Id,
                NumberASP = this.NumberASP,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            FreqMaxKHz = ((FreqRanges)record).FreqMaxKHz;
            FreqMinKHz = ((FreqRanges)record).FreqMinKHz;
            Note = ((FreqRanges)record).Note;
            IsActive = ((FreqRanges)record).IsActive;
        }
    }
}

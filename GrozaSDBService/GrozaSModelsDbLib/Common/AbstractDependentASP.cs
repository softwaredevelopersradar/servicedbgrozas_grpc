﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    public abstract class AbstractDependentASP : AbstractCommonTable
    {
        [DataMember]
        public abstract int NumberASP { get; set; }
    }
}

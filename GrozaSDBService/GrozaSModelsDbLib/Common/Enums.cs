﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public enum NameChangeOperation : byte
    {
        [EnumMember]
        Add,      // Добавить запись 
        [EnumMember]
        Delete,   // Удалить запись
        [EnumMember]
        Change   // Изменить запись
    }

    [DataContract]
    public enum NameTableOperation
    {
        [EnumMember]
        Add,
        [EnumMember]
        AddRange,
        [EnumMember]
        RemoveRange,
        [EnumMember]
        Change,
        [EnumMember]
        ChangeRange,
        [EnumMember]
        Delete,
        [EnumMember]
        Clear,
        [EnumMember]
        Load,
        [EnumMember]
        LoadByFilterAsp,
        [EnumMember]
        ClearByFilter,
        [EnumMember]
        Update,
        [EnumMember]
        None,
        [EnumMember]
        LoadByFilterOwnUAVFreq,
        [EnumMember]
        ClearByFilterOwnUAVFreq
    }

    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        [Description("Станции помех Гроза-С")]
        TableJammerStation,

        [EnumMember]
        [Description("Диапазоны РР")]
        TableFreqRangesRecon,

        [EnumMember]
        [Description("Известные частоты")]
        TableFreqKnown,

        [EnumMember]
        [Description("Запрещенные частоты")]
        TableFreqForbidden,

        [EnumMember]
        [Description("Сектора РР")]
        TableSectorsRecon,

        [EnumMember]
        [Description("ИРИ")]
        TableSource,

        [EnumMember]
        [Description("Траектория ИРИ")]
        TableTrack,

        [EnumMember]
        [Description("Пеленг ИРИ")]
        TableJamBearing,

        [EnumMember]
        [Description("Глобальные настройки")]
        GlobalProperties,

        [EnumMember]
        [Description("ИРИ на подавление")]
        TableSuppressSource,

        [EnumMember]
        [Description("GNSS на подавление")]
        TableSuppressGnss,

        [EnumMember]
        TablePattern,

        [EnumMember]
        [Description("Свои БПЛА")]
        TableOwnUAV,

        [EnumMember]
        [Description("Частоты своих БПЛА")]
        TableOwnUAVFreq,

        [EnumMember]
        [Description("Местоположение пунктов Кирасы-М")]
        TableCuirasseMPoints,

        [EnumMember]
        [Description("Файлы с сигналами Своих БПЛА")]
        TableOwnUAVFreqSignal,

        [EnumMember]
        [Description("Аэроскоп")]
        TableAeroscope,

        [EnumMember]
        [Description("Траектория Аэроскопа БПЛА")]
        TableAeroscopeTrajectory,

        [EnumMember]
        [Description("Гроза-Р")]
        TableOperatorGun
    }

    public enum Languages
    {
        [Description("Русский")]
        Rus,
        [Description("English")]
        Eng
    }

    public enum TypeSource : byte
    {
        [Description("Unknown")]
        Unknown,
        [Description("Lightbridge")]
        Lightbridge,
        [Description("Ocusinc")]
        Ocusinc,
        [Description("Wi-Fi (M)")]
        WiFi,
        [Description("3G")]
        G3
    }

    [DataContract]
    public enum TypeInput : byte
    {
        [EnumMember]
        Auto,
        [EnumMember]
        Manual
    }

    [DataContract]
    public enum StationRole : byte
    {
        [EnumMember]
        Complex,
        [EnumMember]
        Own,
        [EnumMember]
        Linked
    }


    [DataContract]
    public enum TypeGNSS : byte
    {
        [EnumMember]
        Gps = 1,
        [EnumMember]
        Glonass,
        [EnumMember]
        Beidou,
        [EnumMember]
        Galileo
    }

    [DataContract]
    public enum TSSB : byte
    {
        [EnumMember]
        Band160 = 1,
        [EnumMember]
        Band320,
        [EnumMember]
        Band500
    }

    [DataContract]
    public enum ScanChannels : byte
    {
        [EnumMember]
        Ch_1 = 1,
        [EnumMember]
        Ch_2 = 2,
        [EnumMember]
        Ch_1_2 = 3,
        [EnumMember]
        Ch_12 = 4
    }

    [DataContract]
    public enum Modulation : byte
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        KFM = 1,
        [EnumMember]
        LCM = 4,
        [EnumMember]
        LCM_2 = 5
    }

    [DataContract]
    public enum TypeConnectGun : byte
    {
        [EnumMember]
        Modem3G,
        [EnumMember]
        ModemUHF
    
    }
}

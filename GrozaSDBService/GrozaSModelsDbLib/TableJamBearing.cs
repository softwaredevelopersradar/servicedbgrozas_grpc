﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Пеленг ИРИ
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableJamBearing)]
    public class TableJamBearing : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        

        [DataMember]
        public int NumJammer { get; set; }

        [DataMember]
        public float Bearing { get; set; }

        [DataMember]
        public float Level { get; set; }

        [DataMember]
        public float Distance { get; set; }


        [DataMember]
        public int TableTrackId { get; set; }

        [DataMember]
        public int TableSourceId { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            NumJammer = ((TableJamBearing)record).NumJammer;
            Bearing = ((TableJamBearing)record).Bearing;
            Level = ((TableJamBearing)record).Level;
            Distance = ((TableJamBearing)record).Distance;
        }
    }
}

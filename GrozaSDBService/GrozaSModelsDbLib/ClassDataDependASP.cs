﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(AbstractDependentASP))]
    [KnownType(typeof(TableFreqForbidden))]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqKnown))]
    [KnownType(typeof(TableSectorsRecon))]
    public class ClassDataDependASP
    {
        [DataMember]
        public List<AbstractDependentASP> ListRecords { get; set; }

        public ClassDataDependASP()
        {
            ListRecords = new List<AbstractDependentASP>();
        }

        public List<T> ToList<T>() where T : AbstractDependentASP
        {
            return (from t in ListRecords let c = t as T select c).ToList();
        }

        public static ClassDataDependASP ConvertToDataDependASP<T>(List<T> listRecords) where T : class
        {
            ClassDataDependASP objListAbstractData = new ClassDataDependASP();
            if (listRecords == null)
                return null;
            if (listRecords.Count == 0)
                return objListAbstractData;
            objListAbstractData.ListRecords = (from t in listRecords let c = t as AbstractDependentASP select c).ToList();
            return objListAbstractData;
        }
    }
}

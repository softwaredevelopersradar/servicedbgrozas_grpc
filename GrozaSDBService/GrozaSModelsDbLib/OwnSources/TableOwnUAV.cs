﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Windows.Controls.WpfPropertyGrid;
using System.Runtime.CompilerServices;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// ИРИ 
    /// </summary>
    [DataContract]
    [CategoryOrder("Общие", 1)]
    [CategoryOrder("Прочее", 2)]
    [CategoryOrder(nameof(Frequencies), 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableOwnUAV)]
    public class TableOwnUAV : AbstractCommonTable, INotifyPropertyChanged
    {
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
   
        [DataMember]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(1)]
        [DisplayName(nameof(SerialNumber))]
        public string SerialNumber { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(2)]
        [DisplayName(nameof(Name))]
        public string Name { get; set; } = string.Empty;

        private ObservableCollection<TableOwnUAVFreq> frequencies = new ObservableCollection<TableOwnUAVFreq>();
        [DataMember]
        [Category(nameof(Frequencies))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ObservableCollection<TableOwnUAVFreq> Frequencies
        {
            get { return frequencies; }

            set
            {
                if (frequencies != value)
                {
                    frequencies = value;
                    OnPropertyChanged();
                }
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableOwnUAV)record;

            Name = newRecord.Name;
            Note = newRecord.Note;

            Frequencies = Frequencies ?? newRecord.Frequencies;
            if (Frequencies != newRecord.Frequencies)
            {
                Frequencies.Clear();
                foreach (var point in newRecord.Frequencies)
                    Frequencies.Add(point);
            }
        }

        public TableOwnUAV Clone()
        {
            return new TableOwnUAV()
            {
                Id = Id,
                SerialNumber = SerialNumber,
                Name = Name,
                Note = Note,
                Frequencies = Frequencies
            };
        }
    }
}

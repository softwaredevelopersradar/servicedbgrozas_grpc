﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Записи сигнала своих БПЛА
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableOwnUAVFreqSignal)]
    public class TableOwnUavFreqSignal: AbstractCommonTable, INotifyPropertyChanged
    {
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public byte[] BinaryFile { get; set; }

        [DataMember]
        public int TableOwnUAVFreqId { get; set; } 

        public TableOwnUavFreqSignal(byte[] binary, int tableOwnUAVFreqId, int id = 0)
        {
            BinaryFile = binary;
            TableOwnUAVFreqId = tableOwnUAVFreqId;
            Id = id;
        }

        public TableOwnUavFreqSignal()
        { }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableOwnUavFreqSignal)record;

            BinaryFile = newRecord.BinaryFile;
        }


        public TableOwnUavFreqSignal Clone() => new TableOwnUavFreqSignal(BinaryFile, TableOwnUAVFreqId, Id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    [DataContract]
    [CategoryOrder("Общие", 1)]    
    [CategoryOrder("Прочее", 2)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableOperatorGun)]

    public class TableOperatorGun:AbstractCommonTable
    {
        [DataMember]
        [Category("Общие")]
        [DisplayName("IDGun"), ReadOnly(false)]
        [Browsable(true)]
        [PropertyOrder(1)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TypeConnection))]
        [PropertyOrder(3)]
        public TypeConnectGun TypeConnection { get; set; }

       
        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableOperatorGun)record;
            
            Note = newRecord.Note;
            TypeConnection = newRecord.TypeConnection;
          
        }
    


        public TableOperatorGun Clone()
        {
            return new TableOperatorGun
            {
                Id = Id,                
                Note = Note,
                TypeConnection = TypeConnection,
               
            };
        }
    }
}

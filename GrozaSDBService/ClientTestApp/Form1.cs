﻿using GrozaSModelsDBLib;
using GrpcDbClientLib;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Linq;

namespace ClientTestApp
{
    using System.Threading;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {
        ServiceClient clientDB;
        string ip = "127.0.0.1";
        int port = 30051;

        public Form1()
        {
            InitializeComponent();
            this.Name = "TestClient";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBoxTable.DataSource = Enum.GetValues(typeof(NameTable));
        }

        //public void DispatchIfNecessary(Control control, Action action)
        //{
        //    if (control.InvokeRequired)
        //    {
        //        control.Invoke((MethodInvoker)(delegate ()
        //        {
        //            bRole = Convert.ToByte(control.SelectedIndex);

        //        }));

        //    }
        //    else
        //    {
        //        bRole = Convert.ToByte(control.SelectedIndex);

        //    }
        //}

        private void InitClientDB()
        {
            clientDB.OnConnect += ClientDB_OnConnect; ;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
            (clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += TSource_OnUpTable;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += MainWindow_OnUpTable; ;
            (clientDB.Tables[NameTable.TableSource] as ITableUpRecord<TableSource>).OnChangeRecord += MainWindow_OnChangeRecord;
            (clientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += TablePattern_OnUpTable;
            (clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += MainWindow_OnUpTable1; ;
            (clientDB.Tables[NameTable.TableOwnUAVFreqSignal] as ITableUpdate<TableOwnUavFreqSignal>).OnUpTable += Form1_OnUpTable;
            (clientDB.Tables[NameTable.TableAeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += Form1_OnUpTable1; ;
        }

        private void Form1_OnUpTable1(object? sender, TableEventArgs<TableAeroscopeTrajectory> e)
        {
            //if (e.Table.Count == 650 || e.Table.Count == 699)
            //{
                foreach (var rec in e.Table)
                {
                    WriteMess(
                        $" TempAeroTraj: Id: {rec.Id}, SerialNum: {rec.SerialNumber}\n" + Environment.NewLine,
                        Color.Black);
                }
            //}
        }

        private void Form1_OnUpTable(object? sender, TableEventArgs<TableOwnUavFreqSignal> e)
        {
            var t = 8;
        }

        private void HandlerErrorDataBase(object? sender, OperationTableEventArgs e)
        {
            WriteMess(e.GetMessage, Color.Red);
        }

        private void WriteMess(string mess, Color color)
        {
            if (tbMessage.InvokeRequired)
            {
                tbMessage.Invoke((MethodInvoker)(delegate ()
                {
                    tbMessage.ForeColor = color;
                    tbMessage.AppendText(mess);

                }));
            }
            else
            {
                tbMessage.ForeColor = color;
                tbMessage.AppendText(mess);
            }
        }

        private void ChangeConnectBut(string mess, Color color)
        {
            if (buttonConnect.InvokeRequired)
            {
                buttonConnect.Invoke((MethodInvoker)(delegate ()
                {
                    //buttonConnect.ForeColor = color;
                    buttonConnect.Text = mess;

                }));
            }
            else
            {
                //buttonConnect.ForeColor = color;
                buttonConnect.Text = mess;
            }
        }

        private async void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            ChangeConnectBut("Disconnect", Color.Green);
            //DispatchIfNecessary(() =>
            //{
            //    buttonConnect.Text = "Disconnect";
            //});


            foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            {
                var tempTable = await clientDB.Tables[table].LoadAsync<AbstractCommonTable>();
                WriteMess($"Load data from Db. {table.ToString()} count records - {tempTable.Count}" + Environment.NewLine, Color.Black);
                //DispatchIfNecessary(() =>
                //{
                //    tbMessage.AppendText($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
                //});
            }
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            ChangeConnectBut("Connect", Color.Red);
            //DispatchIfNecessary(() =>
            //{
            //    buttonConnect.Text = "Connect";
            //});

            if (eventArgs.GetMessage != "")
            {
                WriteMess(eventArgs.GetMessage, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            WriteMess($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count}" + Environment.NewLine, Color.Black);
            //DispatchIfNecessary(() =>
            //{
            //    tbMessage.ForeColor = Color.Black;

            //    tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            //});
        }

        private void MainWindow_OnUpTable1(object? sender, TableEventArgs<TableOwnUAV> e)
        {
            var t = 6;
        }

        private void TablePattern_OnUpTable(object? sender, TableEventArgs<TablePattern> e)
        {
            var t = 6;
        }

        private void MainWindow_OnChangeRecord(object? sender, TableSource e)
        {
            var t = 6;
        }

        private void MainWindow_OnUpTable(object? sender, TableEventArgs<TableFreqKnown> e)
        {
            var t = 6;
        }

        private void TSource_OnUpTable(object? sender, TableEventArgs<TableSource> e)
        {
            var t = 6;
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ServiceClient(this.Name, ip, port);
                    InitClientDB();
                    clientDB.Connect();
                }
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void buttonAdd1_Click(object sender, EventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
                Random rand = new Random();
                object record = null;
                switch (comboBoxTable.SelectedItem)
                {

                    case NameTable.TableJammerStation:
                        record = new TableJammerStation
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = -53.56d,
                                Longitude = -26.365d
                            },
                            //Coordinates = new Coord
                            //{
                            //    Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            //    Latitude = rand.NextDouble() * rand.Next(0, 360),
                            //    Longitude = rand.NextDouble() * rand.Next(0, 360)
                            //},
                            CallSign = "topol",
                            DeltaTime = "T9",
                            Note = "hello local",
                            Role = StationRole.Linked
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                            AngleMax = Convert.ToInt16(rand.Next(0, 360)),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableSource:
                        record = new TableSource
                        {
                            //Id = rand.Next(1,10),
                            Type = Convert.ToByte(rand.Next(1, 5)),
                            Note = DateTime.Now.ToString(),
                            Track = new ObservableCollection<TableTrack>()
                            {
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                            },
                        };
                        break;
                    case NameTable.TableTrack:
                        record = new TableTrack
                        {
                            TableSourceId = 21,
                            BandMHz = 9
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties()
                        {
                            Id = 1,
                            //Spoofing = new Coord() { Latitude = -250 }
                            Gnss = new Coord() { Latitude = -250, Longitude = -20 }
                            //Num = 0,
                            //Coordinates = new Coord() { Altitude = -1, Latitude = -1, Longitude = -1 },
                            //CompassJ = null,
                            //CompassRI = null,
                            //AutoMode = false,
                            //AutoCourse = true,
                            //CourseAngle = -1,
                            //ZoneAttention = 5000,
                            //ZoneReadiness = 3000,
                            //ZoneAlarm = 1500,
                            //TimeRadiat = 3,
                            //Power100_500 = 125,
                            //Power500_2500 = 50,
                            //Power2500_6000 = 100
                        };
                        break;
                    case NameTable.TableSuppressSource:
                        record = new TableSuppressSource()
                        {
                            FrequencyMHz = rand.NextDouble(),
                            LevelOwn = (short)rand.Next(10),
                            TableSourceId = rand.Next(20),
                            InputType = TypeInput.Manual
                        };
                        break;

                    case NameTable.TablePattern:
                        record = new TablePattern()
                        {
                            //ImageByte = ImageToByteArray( ImageByte.FromFile("E:\\KAKORENKO\\WORK\\3_DJI Mavic 2.png"))
                            FrequencyList = new ObservableCollection<OperatingFrequency>() { new OperatingFrequency() { FrequencyMin = 500, FrequencyMax = 1000, Band = 10 }, new OperatingFrequency() { FrequencyMax = 600, FrequencyMin = 102, Band = 10 } }
                        };
                        break;
                    case NameTable.TableOwnUAV:
                        record = new TableOwnUAV
                        {
                            //Id = rand.Next(1,10),
                            SerialNumber = rand.Next(0, 985555).ToString(),
                            Name = "mavic",
                            Note = DateTime.Now.ToString(),
                            Frequencies = new ObservableCollection<TableOwnUAVFreq>()
                            {
                                new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now},
                                new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now}
                            }
                        };
                        break;
                    case NameTable.TableOwnUAVFreq:
                        record = new TableOwnUAVFreq
                        {
                            FrequencyMHz = rand.NextDouble(),
                            TableOwnUAVId = nufOfRec
                        };
                        break;
                    case NameTable.TableCuirasseMPoints:
                        record = new TableCuirasseMPoints
                        {
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            Note = rand.Next(0, 360).ToString()
                        };
                        break;
                    case NameTable.TableOwnUAVFreqSignal:
                        record = new TableOwnUavFreqSignal
                        {
                            TableOwnUAVFreqId = nufOfRec,
                            BinaryFile = File.ReadAllBytes("E:\\KAKORENKO\\WORK\\22222222222222_10.bin")
                            //new byte[] { 100, 200, 12, 13, 45, 50 }

                        };
                        break;
                    case NameTable.TableAeroscope:
                        record = new TableAeroscope()
                        {
                            SerialNumber = nufOfRec.ToString(),
                            UUIDLength = rand.Next(0,50000), HomeLatitude = rand.Next(0, 180)

                        };
                        break;
                    case NameTable.TableAeroscopeTrajectory:
                        record = new TableAeroscopeTrajectory()
                        {
                            SerialNumber = nufOfRec.ToString(),
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },

                        };
                        break;
                    case NameTable.TableOperatorGun:
                        record = new TableOperatorGun()
                        {
                            Note = "ghgh", TypeConnection = TypeConnectGun.Modem3G

                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].AddAsync(record);
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void buttonAddMany_Click(object sender, EventArgs e)
        {
            try
            {
                Random rand = new Random();
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
                dynamic listForSend = new List<AbstractCommonTable>();

                switch (comboBoxTable.SelectedItem)
                {
                    case NameTable.TableJammerStation:
                        listForSend = new List<TableJammerStation>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableJammerStation record = new TableJammerStation
                            {
                                Coordinates = new Coord
                                {
                                    Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                    Latitude = rand.NextDouble() * rand.Next(0, 360),
                                    Longitude = rand.NextDouble() * rand.Next(0, 360)
                                },
                                CallSign = "topol",
                                //IsOwn = false,
                                Role = StationRole.Own,
                                DeltaTime = "T9",
                                Note = "hello local"
                            };
                            listForSend.Add(record);
                        }
                        break;
                    case NameTable.TableFreqForbidden:
                        listForSend = new List<TableFreqForbidden>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqForbidden record = new TableFreqForbidden
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000)
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        listForSend = new List<TableFreqKnown>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqKnown record = new TableFreqKnown
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        listForSend = new List<TableFreqRangesRecon>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqRangesRecon record = new TableFreqRangesRecon
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        listForSend = new List<TableSectorsRecon>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableSectorsRecon record = new TableSectorsRecon
                            {
                                NumberASP = nufOfRec,
                                AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                                AngleMax = Convert.ToInt16(rand.Next(0, 360)),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableSource:
                        listForSend = new List<TableSource>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableSource record = new TableSource
                            {
                                //Id = rand.Next(1,10),
                                Type = Convert.ToByte(rand.Next(1, 5)),
                                Note = DateTime.Now.ToString(),
                                Track = new ObservableCollection<TableTrack>()
                                {
                                    { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                        Bearing = new ObservableCollection<TableJamBearing>()
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                    }
                                    } },
                                    { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                        Bearing = new ObservableCollection<TableJamBearing>()
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                    }
                                    } },
                                },
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TablePattern:
                        listForSend = new List<TablePattern>();
                        for (int i = 0; i < 3; i++)
                        {
                            TablePattern record = new TablePattern
                            {
                                Id = i + 1,
                                FrequencyList = new ObservableCollection<OperatingFrequency>() { new OperatingFrequency() { FrequencyMin = 500, FrequencyMax = 1000, Band = 10 }, new OperatingFrequency() { FrequencyMax = 600, FrequencyMin = 102, Band = 10 } }
                            };
                            listForSend.Add(record);
                        };
                        break;
                }
                clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].AddRangeAsync(listForSend);
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void buttonChange1_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
            object record = null;
            switch (comboBoxTable.SelectedItem)
            {

                case NameTable.TableJammerStation:
                    record = new TableJammerStation
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord
                        {
                            Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            Latitude = rand.NextDouble() * rand.Next(0, 360),
                            Longitude = rand.NextDouble() * rand.Next(0, 360)
                        },
                        CallSign = "berezka",
                        DeltaTime = "78",
                        Note = "cat",
                        Role = StationRole.Own
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    record = new TableFreqForbidden
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000)
                    };
                    break;
                case NameTable.TableFreqKnown:
                    record = new TableFreqKnown
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    record = new TableFreqRangesRecon
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableSectorsRecon:
                    record = new TableSectorsRecon
                    {
                        Id = nufOfRec,
                        AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                        AngleMax = Convert.ToInt16(rand.Next(0, 360))
                    };
                    break;
                case NameTable.TableSource:
                    record = new TableSource
                    {
                        Id = nufOfRec,
                        Type = Convert.ToByte(rand.Next(1, 5)),
                        Note = DateTime.Now.ToString(),
                        Track = new ObservableCollection<TableTrack>()
                            {
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                            },
                    };
                    break;
                case NameTable.GlobalProperties:
                    record = new GlobalProperties
                    {
                        Id = nufOfRec,
                        //Num = 2
                    };
                    break;
                case NameTable.TableSuppressSource:
                    record = new TableSuppressSource()
                    {
                        Id = nufOfRec,
                        FrequencyMHz = rand.NextDouble(),
                        LevelOwn = (short)rand.Next(10),
                        TableSourceId = rand.Next(20),
                        InputType = TypeInput.Auto
                    };
                    break;
                case NameTable.TableOwnUAV:
                    var table = await clientDB?.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();

                    //table[0].Frequencies.Remove(table[0].Frequencies[0]);
                    //table[0].Frequencies.Add(new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now });
                    //record = table[0];
                    var own = table.FirstOrDefault(x => x.Id == nufOfRec);
                    if (own.Frequencies != null)
                    {
                        own.Frequencies[0].BandMHz = rand.Next(5, 20);
                        own.Frequencies[0].SR = !own.Frequencies[0].SR;
                        //own.Frequencies.Remove(own.Frequencies.Last());
                    }
                    //own.Frequencies.Add(new TableOwnUAVFreq() { FrequencyMHz = rand.Next(100,6000) });
                    record = own;
                    //record = new TableOwnUAV
                    //{
                    //    Id = nufOfRec,
                    //    //SerialNumber = nufOfRec.ToString(),
                    //    //Name = "mavic",
                    //    //Note = DateTime.Now.ToString(),
                    //    Frequencies = new ObservableCollection<TableOwnUAVFreq>()
                    //        {
                    //            new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now},
                    //            new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now}
                    //        }
                    //};
                    break;
                case NameTable.TablePattern:
                    var patterns = await clientDB?.Tables[NameTable.TablePattern].LoadAsync<TablePattern>();

                    patterns[0].FrequencyList.Remove(patterns[0].FrequencyList[0]);
                    patterns[0].FrequencyList.Add(new OperatingFrequency() { FrequencyMin = rand.NextDouble(), FrequencyMax = rand.NextDouble(), Band = rand.Next() });
                    record = patterns[0];
                    break;
                case NameTable.TableOwnUAVFreq:
                    record = new TableOwnUAVFreq
                    {
                        Id = nufOfRec,
                        SR = true
                        //Num = 2
                    };
                    //var ownfreqs = await clientDB?.Tables[NameTable.TableOwnUAVFreq].LoadAsync<TableOwnUAVFreq>();
                    //var own = ownfreqs.FirstOrDefault(x => x.Id == nufOfRec);
                    //if (own != null)
                    //{
                    //    own.SR = !own.SR;
                    //    record = own;
                    //}

                    //patterns[0].FrequencyList.Remove(patterns[0].FrequencyList[0]);
                    //patterns[0].FrequencyList.Add(new OperatingFrequency() { FrequencyMin = rand.NextDouble(), FrequencyMax = rand.NextDouble(), Band = rand.Next() });
                    //record = patterns[0];
                    break;
                default:
                    break;
            }
            if (record != null)
                clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].ChangeAsync(record);

        }

        private void buttonChangeMany_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
            dynamic listForSend = new List<AbstractCommonTable>();
            object record = null;
            switch (comboBoxTable.SelectedItem)
            {

                case NameTable.TableJammerStation:
                    listForSend = new List<TableJammerStation>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableJammerStation
                        {
                            Id = i,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = i,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;

                default:
                    break;
            }
            clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].ChangeRangeAsync(listForSend);

        }

        private void buttonDelete1_Click(object sender, EventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
                Random rand = new Random();
                object record = null;
                switch (comboBoxTable.SelectedItem)
                {

                    case NameTable.TableJammerStation:
                        record = new TableJammerStation
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSource:
                        record = new TableSource
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSuppressSource:
                        record = new TableSuppressSource
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TablePattern:
                        record = new TablePattern()
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableOwnUAV:
                        record = new TableOwnUAV()
                        {
                            SerialNumber = nufOfRec.ToString()
                        };
                        break;
                    case NameTable.TableCuirasseMPoints:
                        record = new TableCuirasseMPoints()
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableAeroscope:
                        record = new TableAeroscope()
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableAeroscopeTrajectory:
                        record = new TableAeroscopeTrajectory()
                        {
                            Id = nufOfRec
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].Delete(record);
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void buttonDeleteMany_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (comboBoxTable.SelectedItem)
            {
                case NameTable.TableJammerStation:
                    listForSend = new List<TableJammerStation>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableJammerStation record = new TableJammerStation
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    }
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqForbidden record = new TableFreqForbidden
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableFreqKnown:
                    listForSend = new List<TableFreqKnown>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqKnown record = new TableFreqKnown
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    listForSend = new List<TableFreqRangesRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqRangesRecon record = new TableFreqRangesRecon
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableSectorsRecon:
                    listForSend = new List<TableSectorsRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableSectorsRecon record = new TableSectorsRecon
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableSource:
                    listForSend = new List<TableSource>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableSource record = new TableSource
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
            }
            if (listForSend != null)
            { clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].RemoveRangeAsync(listForSend); }

        }

        private async void buttonLoad_Click(object sender, EventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);

                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)comboBoxTable.SelectedItem;

                if (clientDB.Tables[nameTable] is IDependentAsp)
                {
                    dynamic tablDepenAsp = await(clientDB.Tables[nameTable] as IDependentAsp).LoadByFilterAsync<FreqRanges>(nufOfRec);
                    WriteMess($"Load data from Db. {((NameTable)comboBoxTable.SelectedItem).ToString()} count records - {tablDepenAsp.Count}" + Environment.NewLine, Color.Black);
                    //DispatchIfNecessary(() =>
                    //{
                    //    tbMessage.AppendText($"Load data from Db. {((NameTable)comboBoxTable.SelectedItem).ToString()} count records - {tablDepenAsp.Count} \n");
                    //});
                }
                else
                {
                    table = await clientDB?.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                    WriteMess($"Load data from Db. {((NameTable)comboBoxTable.SelectedItem).ToString()} count records - {table.Count}" + Environment.NewLine, Color.Black);
                    //DispatchIfNecessary(() =>
                    //{
                    //    tbMessage.AppendText($"Load data from Db. {((NameTable)comboBoxTable.SelectedItem).ToString()} count records - {table.Count} \n");
                    //});

                    //if (nameTable == NameTable.TablePattern)
                    //{
                    //    //if (table.Count > 0)
                    //    //{ Img.Source = (table[0] as TablePattern).Image; }
                    //    if (table.Count > 0)
                    //    { Img.Source = ImageOperating.ConvertToImageSource((table[0] as TablePattern).ImageByte); }

                    //}
                }


            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                WriteMess(excpetService.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(excpetService.Message);
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            try
            {
                clientDB?.Tables[(NameTable)comboBoxTable.SelectedItem].Clear();
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            clientDB?.Dispose();
        }

        private void buttonLoadFilter_Click(object sender, EventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
                NameTable nameTable = NameTable.TableOwnUAVFreqSignal;

                var sinal = (clientDB?.Tables[nameTable] as IDependentOwnUavFreq).LoadByFilter(nufOfRec);
                WriteMess($"Load data from Db. {(NameTable.TableOwnUAVFreqSignal).ToString()}" + Environment.NewLine, Color.Black);
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
            }
            catch (ExceptionDatabase excpetService)
            {
                WriteMess(excpetService.Message, Color.Red);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(textBoxNumRec.Text);
                Random rand = new Random();
                object record = null;

                int i = 0;
                while (i < 30)
                {
                        record = new TableAeroscopeTrajectory()
                        {
                            SerialNumber = nufOfRec.ToString(),
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = 5.3 + rand.Next(0, 360) * 0.0001d,
                                Longitude = 2.7 + rand.Next(0, 360) * 0.0001d,
                            },
                            Yaw = rand.Next(0, 360), Num = i
                        };
                        clientDB?.Tables[NameTable.TableAeroscopeTrajectory].AddAsync(record);
                        i++;
                        Task.Delay(2000);
                }
                   
            }
            catch (ExceptionClient exceptClient)
            {
                WriteMess(exceptClient.Message, Color.Red);
                //tbMessage.ForeColor = Color.Red;
                //tbMessage.AppendText(exceptClient.Message);
            }
        }

        //Console.WriteLine("Hello World!");
        //    //var testProto = new CoordMessage() { Altitude = 250, Latitude = 56.2345, Longitude = 88.84964 };
        //    //var testProto = new JammerMessage() { Id = 1, DeltaTime="23", IsGnssUsed = true, Note = "ghfh", Coordinates = new CoordMessage() { Altitude = 250, Latitude = 56.2345, Longitude = 88.84964 }, Role = JammerMessage.Types.EnumRole.Own, CallSign = "gigi" };
        //    //var testProto = new FreqRangeMessage() {  FreqMinKHz = 2356, FreqMaxKHz = 5632.2, NumberASP = 45, IsActive = false, Note = "jhgj", Id = 1 };
        //    //var testProto = new SectorMessage() { AngleMin = 2356, AngleMax = 5632, NumberASP = 45, IsActive = false,Id = 1 };
        //    //var testProto = new CuirasseMMessage() { Coordinates = new CoordMessage() { Altitude = 250, Latitude = 56.2345, Longitude = 88.84964 }, Id = 1 };
        //    //var testProto = new SourceMessage() { Id = 1, TypeRSM = 1 };
        //    //var track1 = new TrackMessage() { FrequencyMHz = 2530, Time = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()) };
        //    //var bearing1 = new BearingMessage() { Bearing = 10, Id = 1 }; var bearing2 = new BearingMessage() { Bearing = 20, Id = 2 };
        //    //track1.Bearing.Add(bearing1); track1.Bearing.Add(bearing2);
        //    //testProto.Track.Add(track1);
        //    //var ownFreq = new OwnUAVFreqMessage() { Id = 3, Time = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), FrequencyMHz = 4560 };
        //    //var testProto = new GlobalSettingsMessage() { Id = 1,  Gnss = new CoordMessage() { Altitude = 250, Latitude = 56.2345, Longitude = 88.84964 }, RadioIntelegence = new RadioIntelegenceMessage() {  CourseAngle = 20, SSB=RadioIntelegenceMessage.Types.EnumTSSB.Band500}, Oem= new ParamOemMessage() {  Distance=1000} };
        //    //var testProto = new SuppressSourceMessage() { Id = 1, FrequencyMHz = 2000, Modulation = SuppressSourceMessage.Types.EnumModulation.Kfm };
        //    //var testProto = new SuppressGnssMessage() { Id = 1,  L1=true, Type= SuppressGnssMessage.Types.EnumGnssType.Galileo };
        //    //var freq1 = new OperatingFrequencyMessage() { FrequencyMax = 2600 }; var freq2 = new OperatingFrequencyMessage() { FrequencyMax = 3600 };
        //    //var testProto = new PatternMessage() { Id = 1,  Name = "kjb", ImageByte = Google.Protobuf.ByteString.CopyFrom(new byte[] {26, 29,32 })};
        //    //testProto.FrequencyList.Add(freq1); testProto.FrequencyList.Add(freq2); 
        //    //var freq1 = new OwnUAVFreqMessage() { FrequencyMHz = 2600, Time = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()) }; 
        //    //var testProto = new OwnUAVMessage() { Id = 1, Name = "kjb", SerialNumber = "vgvhfcxdf" };
        //    //testProto.Frequencies.Add(freq1);
        //    var testProto = new OwnUAVFreqSignalMessage() { Id = 1, BinaryFile = Google.Protobuf.ByteString.CopyFrom(new byte[] { 26, 29, 32 }) };
        //var resultModel = TypesConverter.ConvertToDBModel(testProto);
        //var resultProto = TypesConverter.ConvertToProto(resultModel);
        //Console.ReadLine();
    }
}

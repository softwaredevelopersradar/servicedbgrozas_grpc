﻿namespace ClientTestApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnect = new System.Windows.Forms.Button();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNumRec = new System.Windows.Forms.TextBox();
            this.tbMessage = new System.Windows.Forms.TextBox();
            this.buttonAdd1 = new System.Windows.Forms.Button();
            this.buttonAddMany = new System.Windows.Forms.Button();
            this.buttonChangeMany = new System.Windows.Forms.Button();
            this.buttonChange1 = new System.Windows.Forms.Button();
            this.buttonDeleteMany = new System.Windows.Forms.Button();
            this.buttonDelete1 = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonLoadFilter = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonConnect.Location = new System.Drawing.Point(63, 26);
            this.buttonConnect.Margin = new System.Windows.Forms.Padding(1);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(112, 33);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.ItemHeight = 17;
            this.comboBoxTable.Location = new System.Drawing.Point(183, 29);
            this.comboBoxTable.Margin = new System.Windows.Forms.Padding(1);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(178, 25);
            this.comboBoxTable.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Aqua;
            this.label1.Location = new System.Drawing.Point(380, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "ID of record";
            // 
            // textBoxNumRec
            // 
            this.textBoxNumRec.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxNumRec.Location = new System.Drawing.Point(470, 26);
            this.textBoxNumRec.Margin = new System.Windows.Forms.Padding(1);
            this.textBoxNumRec.Name = "textBoxNumRec";
            this.textBoxNumRec.Size = new System.Drawing.Size(105, 25);
            this.textBoxNumRec.TabIndex = 3;
            this.textBoxNumRec.Text = "1";
            // 
            // tbMessage
            // 
            this.tbMessage.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbMessage.Location = new System.Drawing.Point(29, 160);
            this.tbMessage.Margin = new System.Windows.Forms.Padding(1);
            this.tbMessage.Multiline = true;
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(738, 275);
            this.tbMessage.TabIndex = 4;
            // 
            // buttonAdd1
            // 
            this.buttonAdd1.Location = new System.Drawing.Point(28, 78);
            this.buttonAdd1.Margin = new System.Windows.Forms.Padding(1);
            this.buttonAdd1.Name = "buttonAdd1";
            this.buttonAdd1.Size = new System.Drawing.Size(154, 27);
            this.buttonAdd1.TabIndex = 5;
            this.buttonAdd1.Text = "Add 1 record";
            this.buttonAdd1.UseVisualStyleBackColor = true;
            this.buttonAdd1.Click += new System.EventHandler(this.buttonAdd1_Click);
            // 
            // buttonAddMany
            // 
            this.buttonAddMany.Location = new System.Drawing.Point(28, 108);
            this.buttonAddMany.Margin = new System.Windows.Forms.Padding(1);
            this.buttonAddMany.Name = "buttonAddMany";
            this.buttonAddMany.Size = new System.Drawing.Size(154, 27);
            this.buttonAddMany.TabIndex = 6;
            this.buttonAddMany.Text = "Add range";
            this.buttonAddMany.UseVisualStyleBackColor = true;
            this.buttonAddMany.Click += new System.EventHandler(this.buttonAddMany_Click);
            // 
            // buttonChangeMany
            // 
            this.buttonChangeMany.Location = new System.Drawing.Point(207, 108);
            this.buttonChangeMany.Margin = new System.Windows.Forms.Padding(1);
            this.buttonChangeMany.Name = "buttonChangeMany";
            this.buttonChangeMany.Size = new System.Drawing.Size(154, 27);
            this.buttonChangeMany.TabIndex = 8;
            this.buttonChangeMany.Text = "Change range";
            this.buttonChangeMany.UseVisualStyleBackColor = true;
            this.buttonChangeMany.Click += new System.EventHandler(this.buttonChangeMany_Click);
            // 
            // buttonChange1
            // 
            this.buttonChange1.Location = new System.Drawing.Point(207, 78);
            this.buttonChange1.Margin = new System.Windows.Forms.Padding(1);
            this.buttonChange1.Name = "buttonChange1";
            this.buttonChange1.Size = new System.Drawing.Size(154, 27);
            this.buttonChange1.TabIndex = 7;
            this.buttonChange1.Text = "Change 1 record";
            this.buttonChange1.UseVisualStyleBackColor = true;
            this.buttonChange1.Click += new System.EventHandler(this.buttonChange1_Click);
            // 
            // buttonDeleteMany
            // 
            this.buttonDeleteMany.Location = new System.Drawing.Point(390, 108);
            this.buttonDeleteMany.Margin = new System.Windows.Forms.Padding(1);
            this.buttonDeleteMany.Name = "buttonDeleteMany";
            this.buttonDeleteMany.Size = new System.Drawing.Size(154, 27);
            this.buttonDeleteMany.TabIndex = 10;
            this.buttonDeleteMany.Text = "Delete range";
            this.buttonDeleteMany.UseVisualStyleBackColor = true;
            this.buttonDeleteMany.Click += new System.EventHandler(this.buttonDeleteMany_Click);
            // 
            // buttonDelete1
            // 
            this.buttonDelete1.Location = new System.Drawing.Point(390, 78);
            this.buttonDelete1.Margin = new System.Windows.Forms.Padding(1);
            this.buttonDelete1.Name = "buttonDelete1";
            this.buttonDelete1.Size = new System.Drawing.Size(154, 27);
            this.buttonDelete1.TabIndex = 9;
            this.buttonDelete1.Text = "Delete 1 record";
            this.buttonDelete1.UseVisualStyleBackColor = true;
            this.buttonDelete1.Click += new System.EventHandler(this.buttonDelete1_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(611, 108);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(1);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(154, 27);
            this.buttonClear.TabIndex = 12;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(611, 78);
            this.buttonLoad.Margin = new System.Windows.Forms.Padding(1);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(69, 27);
            this.buttonLoad.TabIndex = 11;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonLoadFilter
            // 
            this.buttonLoadFilter.Location = new System.Drawing.Point(682, 78);
            this.buttonLoadFilter.Margin = new System.Windows.Forms.Padding(1);
            this.buttonLoadFilter.Name = "buttonLoadFilter";
            this.buttonLoadFilter.Size = new System.Drawing.Size(83, 27);
            this.buttonLoadFilter.TabIndex = 13;
            this.buttonLoadFilter.Text = "Load filter";
            this.buttonLoadFilter.UseVisualStyleBackColor = true;
            this.buttonLoadFilter.Click += new System.EventHandler(this.buttonLoadFilter_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(682, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(800, 442);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonLoadFilter);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonDeleteMany);
            this.Controls.Add(this.buttonDelete1);
            this.Controls.Add(this.buttonChangeMany);
            this.Controls.Add(this.buttonChange1);
            this.Controls.Add(this.buttonAddMany);
            this.Controls.Add(this.buttonAdd1);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.textBoxNumRec);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxTable);
            this.Controls.Add(this.buttonConnect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNumRec;
        private System.Windows.Forms.TextBox tbMessage;
        private System.Windows.Forms.Button buttonAdd1;
        private System.Windows.Forms.Button buttonAddMany;
        private System.Windows.Forms.Button buttonChangeMany;
        private System.Windows.Forms.Button buttonChange1;
        private System.Windows.Forms.Button buttonDeleteMany;
        private System.Windows.Forms.Button buttonDelete1;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonLoadFilter;
        private System.Windows.Forms.Button button1;
    }
}
